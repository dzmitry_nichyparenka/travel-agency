INSERT INTO "public".countries (name) VALUES ('China'), ('USA'), ('India'), ('Australia'), ('Brazil'), ('Lithuania');

INSERT INTO "public".hotels (country_id, name, phone, stars) VALUES
(1, 'Yindo Hotel Zhuhai', '+86 756 888 3388', 5),
(2, 'Holiday Inn Totowa Wayne', '+1 973 785 9000', 3),
(3, 'Oberoi Trident Towers', '+91 22 6632 5757', 5),
(2, 'Crowne Plaza Fairfield', '+1 973 227 9200', 3),
(6, 'Forrest Hotel and Apartments', '+61 2 6203 4300', 3),
(4, 'Ibis', '+55 51 4009-4888', 3);

INSERT INTO "public".tours (country_id, hotel_id, type, name, description, photo, start_date, duration, cost) VALUES
(1, 1, 'EXCURSION', 'Excursion tour to China', 'Excursion tour to China', NULL, '2018-04-15', 5, 950.50), 
(2, 2, 'BUSINESS', 'Business tour to USA', 'Just business tour', NULL, '2018-04-23', 3, 370.30), 
(3, 3, 'RECREATION', 'Recreation tour to India', 'Amazing recreation tour', NULL, '2018-04-28', 7, 835.00);

INSERT INTO "public".users (login, password, email, first_name, last_name) VALUES 
('excelsiorious', '7c6a180b36896a0a8c02787eeafb0e4c', 'excelsiorious@gmail.com', 'Dmitry', 'Nichiporenko'), 
('petr0v48artem', '6cb75f652a9b52798eb6cf2201057c73', 'pert_petrov456410@gmail.com', 'Petr', 'Petrov'), 
('a.nickiforov', '819b0643d6b89dc9b579fdfc9094f28e', 'artem_nickiforov0012@gmail.com', 'Artem', 'Nickiforov');

INSERT INTO "public".reviews (tour_id, user_id, text) VALUES
(1, 1, NULL), (1, 2, NULL), (1, 3, NULL), 
(2, 1, NULL), (2, 3, NULL), 
(3, 2, NULL);