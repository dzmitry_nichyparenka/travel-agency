CREATE SCHEMA IF NOT EXISTS "public";

DROP TABLE IF EXISTS reviews;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS tours;
DROP TABLE IF EXISTS hotels;
DROP TABLE IF EXISTS countries;
DROP TYPE IF EXISTS tour_type;

CREATE TABLE IF NOT EXISTS "public".countries (
	id								bigserial			PRIMARY KEY,
	name							varchar(30)		NOT NULL
);

CREATE TABLE IF NOT EXISTS "public".hotels (
	id								bigserial 		PRIMARY KEY,
	country_id				bigint				NOT NULL,
	name							varchar(50)  	NOT NULL,
	phone							varchar(30)  	NOT NULL,
	stars							smallint
);

CREATE TYPE tour_type AS ENUM('RECREATION', 'EXCURSION', 'SPORTS', 'HEALTH', 'BUSINESS');

CREATE TABLE IF NOT EXISTS "public".tours (
	id								BIGSERIAL			PRIMARY KEY,
	country_id				bigint				NOT NULL,
	hotel_id					bigint,
	type							tour_type			NOT NULL,
	name							varchar(50)		NOT NULL,
	description				varchar(150)	NOT NULL,
	photo							bytea,
	start_date				timestamp			NOT NULL,
	duration					smallint			NOT NULL,
	cost							numeric(6,2)	NOT NULL
);

CREATE TABLE IF NOT EXISTS "public".users (
	id								bigserial 		PRIMARY KEY,
	login							varchar(50)  	NOT NULL,
	password					varchar(50)  	NOT NULL,
	email							varchar(50)  	NOT NULL,
	first_name				varchar(30)  	NOT NULL,
	last_name					varchar(30)  	NOT NULL
);

CREATE TABLE IF NOT EXISTS "public".reviews (
	id								bigserial 		PRIMARY KEY,
	tour_id						bigint 				NOT NULL,
	user_id						bigint 				NOT NULL,
	text							varchar(250)
);

ALTER TABLE "public".hotels DROP CONSTRAINT IF EXISTS fk_hotels_countries;
ALTER TABLE "public".hotels ADD CONSTRAINT fk_hotels_countries FOREIGN KEY (country_id) REFERENCES "public".countries(id) ON DELETE RESTRICT;
ALTER TABLE "public".reviews DROP CONSTRAINT IF EXISTS fk_reviews_tours;
ALTER TABLE "public".reviews ADD CONSTRAINT fk_reviews_tours FOREIGN KEY (tour_id) REFERENCES "public".tours(id) ON DELETE CASCADE;
ALTER TABLE "public".reviews DROP CONSTRAINT IF EXISTS fk_reviews_users;
ALTER TABLE "public".reviews ADD CONSTRAINT fk_reviews_users FOREIGN KEY (user_id) REFERENCES "public".users(id) ON DELETE CASCADE;
ALTER TABLE "public".tours DROP CONSTRAINT IF EXISTS fk_tours_countries;
ALTER TABLE "public".tours ADD CONSTRAINT fk_tours_countries FOREIGN KEY (country_id) REFERENCES "public".countries(id) ON DELETE RESTRICT;
ALTER TABLE "public".tours DROP CONSTRAINT IF EXISTS fk_tours_hotels;
ALTER TABLE "public".tours ADD CONSTRAINT fk_tours_hotels FOREIGN KEY (hotel_id) REFERENCES "public".hotels(id) ON DELETE RESTRICT;