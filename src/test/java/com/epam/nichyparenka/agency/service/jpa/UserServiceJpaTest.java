package com.epam.nichyparenka.agency.service.jpa;

import com.epam.nichyparenka.agency.configuration.JpaConfiguration;
import com.epam.nichyparenka.agency.entity.Country;
import com.epam.nichyparenka.agency.entity.Hotel;
import com.epam.nichyparenka.agency.entity.User;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.service.CountryService;
import com.epam.nichyparenka.agency.service.HotelService;
import com.epam.nichyparenka.agency.service.UserService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JpaConfiguration.class})
@ActiveProfiles({"jpa", "test"})
public class UserServiceJpaTest {
    private static final String COUNTRY_NAME = "Canada";
    private static final String HOTEL_NAME = "Example hotel";
    private static final String HOTEL_PHONE_NUMBER = "+375 33 3638375";
    private static final byte HOTEL_STARS_COUNT = 5;
    private static final String USER_LOGIN_1 = "excelsiorious";
    private static final String USER_LOGIN_2 = "anotherlogin";
    private static final String USER_BAD_LOGIN = "bad";
    private static final String USER_PASSWORD = "7c6a180b36896a0a8c02787eeafb0e4c";
    private static final String USER_EMAIL = "excelsiorious@gmail.com";
    private static final String USER_FIRST_NAME = "Dmitry";
    private static final String USER_LAST_NAME = "Nichiporenko";
    private static final int ID_INCORRECT = -100;
    private static Country country = new Country();
    private static Hotel hotel = new Hotel();
    private static User user = new User();

    @Autowired
    @Qualifier("countryService")
    private CountryService countryService;

    @Autowired
    @Qualifier("hotelService")
    private HotelService hotelService;

    @Autowired
    @Qualifier("userService")
    private UserService userService;

    @BeforeClass
    public static void beforeClass() {
        hotel.setCountry(country);
        hotel.setPhoneNumber(HOTEL_PHONE_NUMBER);
        hotel.setNumberOfStars(HOTEL_STARS_COUNT);
        hotel.setName(HOTEL_NAME);

        country.setName(COUNTRY_NAME);

        user.setLogin(USER_LOGIN_1);
        user.setPassword(USER_PASSWORD);
        user.setEmail(USER_EMAIL);
        user.setFirstName(USER_FIRST_NAME);
        user.setLastName(USER_LAST_NAME);
    }

    @Test
    public void testUserCreateTrue() {
        user.setId(0);
        assertTrue(userService.createUser(user));
    }

    @Test
    public void testUserCreateFalse() {
        user.setId(0);
        user.setLogin(USER_BAD_LOGIN);
        try {
            userService.createUser(user);
            fail();
        } catch (Exception exception) {
            assertTrue(true);
        }
    }

    @Test
    public void testUserReadTrue() {
        user.setId(0);
        try {
            userService.createUser(user);
            Optional<User> userOptional = userService.readUser(user.getId());
            if (userOptional.isPresent()) {
                assertTrue(true);
            } else {
                fail();
            }
        } catch (Exception exception) {
            fail();
        }
    }

    @Test
    public void testUserReadFalse() {
        Optional<User> userOptional = userService.readUser(ID_INCORRECT);
        if (userOptional.isPresent()) {
            fail();
        } else {
            assertTrue(true);
        }
    }

    @Test
    public void testUserUpdateTrue() {
        user.setId(0);

        countryService.createCountry(country);
        hotelService.createHotel(hotel);
        userService.createUser(user);

        user.setLogin(USER_LOGIN_2);

        boolean result = userService.updateUser(user);
        if (result) {
            Optional<User> userOptional = userService.readUser(user.getId());
            if (userOptional.isPresent()) {
                assertEquals(user.getLogin(), userOptional.get().getLogin());
            } else {
                fail();
            }
        } else {
            fail();
        }
    }

    @Test
    public void testUserUpdateFalse() {
        user.setLogin(USER_BAD_LOGIN);
        try {
            userService.updateUser(user);
            fail();
        } catch (Exception exception) {
            assertTrue(true);
        }
    }

    @Test
    public void testUserDeleteTrue() {
        user.setId(0);
        user.setLogin(USER_LOGIN_1);
        userService.createUser(user);
        assertTrue(userService.deleteUser(user));
    }

    @Test
    public void testUserDeleteFalse() {
        user.setId(ID_INCORRECT);
        user.setLogin(USER_LOGIN_1);
        assertFalse(userService.deleteUser(user));
    }

    @Test
    public void testUsersDeleteTrue() {
        User user = new User();
        user.setLogin(USER_LOGIN_1);
        user.setPassword(USER_PASSWORD);
        user.setEmail(USER_EMAIL);
        user.setFirstName(USER_FIRST_NAME);
        user.setLastName(USER_LAST_NAME);

        User user2 = new User();
        user2.setLogin(USER_LOGIN_1);
        user2.setPassword(USER_PASSWORD);
        user2.setEmail(USER_EMAIL);
        user2.setFirstName(USER_FIRST_NAME);
        user2.setLastName(USER_LAST_NAME);

        userService.createUser(user);
        userService.createUser(user2);

        try {
            userService.deleteUsers(user, user2);
        } catch (BadQueryExecutionException exception) {
            fail();
        }
    }

    @Test
    public void testUsersDeleteFalse() {
        User user = new User();
        user.setLogin(USER_LOGIN_1);
        user.setPassword(USER_PASSWORD);
        user.setEmail(USER_EMAIL);
        user.setFirstName(USER_FIRST_NAME);
        user.setLastName(USER_LAST_NAME);

        User user2 = new User();
        user2.setLogin(USER_LOGIN_1);
        user2.setPassword(USER_PASSWORD);
        user2.setEmail(USER_EMAIL);
        user2.setFirstName(USER_FIRST_NAME);
        user2.setLastName(USER_LAST_NAME);

        userService.createUser(user);
        userService.createUser(user2);

        user.setId(ID_INCORRECT);
        user2.setId(ID_INCORRECT);

        try {
            userService.deleteUsers(user, user2);
            fail();
        } catch (BadQueryExecutionException exception) {
            assertTrue(true);
        }
    }
}
