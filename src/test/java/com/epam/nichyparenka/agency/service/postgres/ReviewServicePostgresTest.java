package com.epam.nichyparenka.agency.service.postgres;

import com.epam.nichyparenka.agency.configuration.PostgresConfiguration;
import com.epam.nichyparenka.agency.entity.Review;
import com.epam.nichyparenka.agency.entity.Tour;
import com.epam.nichyparenka.agency.entity.User;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.service.ReviewService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PostgresConfiguration.class})
@ActiveProfiles({"postgres", "test"})
public class ReviewServicePostgresTest {
    private static final String REVIEW_TEXT_1 = "Text example";
    private static final String REVIEW_TEXT_2 = "Text example 2";
    private static final int ID_INCORRECT = -100;
    private static Review review = new Review();

    @Autowired
    @Qualifier("reviewService")
    private ReviewService reviewService;

    @BeforeClass
    public static void beforeClass() {
        Tour tour = new Tour();
        User user = new User();
        tour.setId(1);
        user.setId(1);

        review.setId(3);
        review.setTour(tour);
        review.setUser(user);
        review.setText(REVIEW_TEXT_1);
    }

    @Test
    public void testReviewCreateTrue() {
        assertTrue(reviewService.createReview(review));
    }

    @Test
    public void testReviewCreateFalse() {
        Review review2 = new Review();
        review2.setId(ID_INCORRECT);
        assertFalse(reviewService.createReview(review2));
    }

    @Test
    public void testReviewReadTrue() {
        review.setId(1);

        User user = new User();
        user.setId(1);
        Tour tour = new Tour();
        tour.setId(1);

        review.setText(null);
        review.setUser(user);
        review.setTour(tour);

        reviewService.createReview(review);
        Optional<Review> reviewOptional = reviewService.readReview(review.getId());
        if (reviewOptional.isPresent()) {
            Review anotherReview = reviewOptional.get();
            assertEquals(review, anotherReview);
        } else {
            fail();
        }
    }

    @Test
    public void testReviewReadFalse() {
        Optional<Review> reviewOptional = reviewService.readReview(ID_INCORRECT);
        if (reviewOptional.isPresent()) {
            fail();
        } else {
            assertNotEquals(null, review);
        }
    }

    @Test
    public void testReviewUpdateTrue() {
        review.setText(REVIEW_TEXT_1);
        reviewService.createReview(review);

        Review newReview = new Review();
        newReview.setId(3);
        newReview.setText(REVIEW_TEXT_2);

        assertTrue(reviewService.updateReview(review));
    }

    @Test
    public void testReviewUpdateFalse() {
        Review newReview = new Review();
        newReview.setId(ID_INCORRECT);
        newReview.setText(REVIEW_TEXT_1);

        User user = new User();
        user.setId(1);
        Tour tour = new Tour();
        tour.setId(1);

        newReview.setTour(tour);
        newReview.setUser(user);

        assertFalse(reviewService.updateReview(newReview));
    }

    @Test
    public void testReviewDeleteTrue() {
        review.setId(5);
        assertTrue(reviewService.deleteReview(review));
    }

    @Test
    public void testReviewDeleteFalse() {
        Review review = new Review();
        review.setId(ID_INCORRECT);

        assertFalse(reviewService.deleteReview(review));
    }

    @Test
    public void testReviewsDeleteTrue() {
        Review review = new Review();
        review.setId(6);
        Review review2 = new Review();
        review2.setId(7);
        reviewService.createReview(review);
        reviewService.createReview(review2);
        try {
            reviewService.deleteReviews(review, review2);
            assertTrue(true);
        } catch (BadQueryExecutionException exception) {
            fail();
        }
    }

    @Test
    public void testReviewsDeleteFalse() {
        Review review = new Review();
        review.setId(ID_INCORRECT);
        Review review2 = new Review();
        review2.setId(ID_INCORRECT);
        try {
            reviewService.deleteReviews(review, review2);
            fail();
        } catch (BadQueryExecutionException exception) {
            assertTrue(true);
        }
    }
}