package com.epam.nichyparenka.agency.service.jpa;

import com.epam.nichyparenka.agency.configuration.JpaConfiguration;
import com.epam.nichyparenka.agency.entity.Country;
import com.epam.nichyparenka.agency.entity.Hotel;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.service.CountryService;
import com.epam.nichyparenka.agency.service.HotelService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JpaConfiguration.class})
@ActiveProfiles({"jpa", "test"})
public class HotelServiceJpaTest {
    private static final String COUNTRY_NAME_1 = "Canada";
    private static final String COUNTRY_NAME_2 = "Japan";
    private static final String HOTEL_NAME_1 = "Example hotel";
    private static final String HOTEL_NAME_2 = "Holiday Inn Totowa Wayne";
    private static final String HOTEL_PHONE_NUMBER = "+375 33 3638375";
    private static final byte HOTEL_STARS_COUNT = 3;
    private static final int ID_INCORRECT = -100;
    private static Country country = new Country();
    private static Hotel hotel = new Hotel();

    @Autowired
    @Qualifier("countryService")
    private CountryService countryService;

    @Autowired
    @Qualifier("hotelService")
    private HotelService hotelService;

    @BeforeClass
    public static void beforeClass() {
        country.setName(COUNTRY_NAME_1);
        hotel.setName(HOTEL_NAME_1);
        hotel.setPhoneNumber(HOTEL_PHONE_NUMBER);
        hotel.setNumberOfStars(HOTEL_STARS_COUNT);
        hotel.setCountry(country);
    }

    @Test
    public void testHotelCreateTrue() {
        Country country = new Country();
        country.setName(COUNTRY_NAME_2);
        countryService.createCountry(country);
        Hotel hotel = new Hotel();
        hotel.setName(HOTEL_NAME_1);
        hotel.setPhoneNumber(HOTEL_PHONE_NUMBER);
        hotel.setNumberOfStars(HOTEL_STARS_COUNT);
        hotel.setCountry(country);
        assertTrue(hotelService.createHotel(hotel));
    }

    @Test
    public void testHotelCreateFalse() {
        Country country = new Country();
        country.setName(COUNTRY_NAME_2);
        countryService.createCountry(country);
        Hotel hotel = new Hotel();
        hotel.setId(ID_INCORRECT);
        hotel.setName(HOTEL_NAME_1);
        hotel.setPhoneNumber(HOTEL_PHONE_NUMBER);
        hotel.setNumberOfStars(HOTEL_STARS_COUNT);
        hotel.setCountry(country);
        try {
            hotelService.createHotel(hotel);
            fail();
        } catch (Exception exception) {
            assertTrue(true);
        }
    }

    @Test
    public void testHotelReadTrue() {
        Optional<Hotel> optionalHotel = hotelService.readHotel(hotel.getId());
        if (optionalHotel.isPresent()) {
            assertEquals(hotel.getName(), optionalHotel.get().getName());
        } else {
            fail();
        }
    }

    @Test
    public void testHotelReadFalse() {
        Optional<Hotel> optionalHotel = hotelService.readHotel(ID_INCORRECT);
        if (optionalHotel.isPresent()) {
            fail();
        } else {
            assertTrue(true);
        }
    }

    @Test
    public void testHotelUpdateTrue() {
        countryService.createCountry(country);
        hotelService.createHotel(hotel);
        hotel.setName(HOTEL_NAME_2);
        boolean result = hotelService.updateHotel(hotel);
        if (result) {
            Optional<Hotel> hotelOptional = hotelService.readHotel(hotel.getId());
            if (hotelOptional.isPresent()) {
                assertEquals(hotel.getName(), hotelOptional.get().getName());
            } else {
                fail();
            }
        } else {
            fail();
        }
    }

    @Test
    public void testHotelUpdateFalse() {
        hotel.setId(ID_INCORRECT);
        boolean result = hotelService.updateHotel(hotel);
        if (result) {
            fail();
        } else {
            assertTrue(true);
        }
    }

    @Test
    public void testHotelDeleteTrue() {
        boolean result = hotelService.deleteHotel(hotel);
        if (result) {
            Optional<Hotel> hotelOptional = hotelService.readHotel(hotel.getId());
            if (hotelOptional.isPresent()) {
                fail();
            } else {
                assertTrue(true);
            }
        } else {
            fail();
        }
    }

    @Test
    public void testHotelDeleteFalse() {
        Hotel hotel = new Hotel();
        hotel.setId(ID_INCORRECT);
        hotel.setName(HOTEL_NAME_1);
        hotel.setPhoneNumber(HOTEL_PHONE_NUMBER);
        assertFalse(hotelService.deleteHotel(hotel));
    }

    @Test
    public void testHotelsDeleteTrue() {
        Country country1 = new Country();
        country1.setName(COUNTRY_NAME_1);
        countryService.createCountry(country1);

        Hotel hotel1 = new Hotel();
        hotel1.setName(HOTEL_NAME_1);
        hotel1.setCountry(country1);
        hotel1.setPhoneNumber(HOTEL_PHONE_NUMBER);
        hotel1.setNumberOfStars(HOTEL_STARS_COUNT);

        Hotel hotel2 = new Hotel();
        hotel2.setName(HOTEL_NAME_1);
        hotel2.setCountry(country1);
        hotel2.setPhoneNumber(HOTEL_PHONE_NUMBER);
        hotel2.setNumberOfStars(HOTEL_STARS_COUNT);

        hotelService.createHotel(hotel1);
        hotelService.createHotel(hotel2);

        try {
            hotelService.deleteHotels(hotel1, hotel2);
        } catch (BadQueryExecutionException exception) {
            fail();
        }
    }

    @Test
    public void testHotelsDeleteFalse() {
        Country country1 = new Country();
        country1.setName(COUNTRY_NAME_1);
        countryService.createCountry(country1);

        Hotel hotel1 = new Hotel();
        hotel1.setName(HOTEL_NAME_1);
        hotel1.setCountry(country1);
        hotel1.setPhoneNumber(HOTEL_PHONE_NUMBER);
        hotel1.setNumberOfStars(HOTEL_STARS_COUNT);

        Hotel hotel2 = new Hotel();
        hotel2.setName(HOTEL_NAME_1);
        hotel2.setCountry(country1);
        hotel2.setPhoneNumber(HOTEL_PHONE_NUMBER);
        hotel2.setNumberOfStars(HOTEL_STARS_COUNT);

        hotelService.createHotel(hotel1);
        hotelService.createHotel(hotel2);

        hotel1.setId(ID_INCORRECT);
        hotel2.setId(ID_INCORRECT);

        try {
            hotelService.deleteHotels(hotel1, hotel2);
            fail();
        } catch (BadQueryExecutionException exception) {
            assertTrue(true);
        }
    }
}
