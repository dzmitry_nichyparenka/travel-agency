package com.epam.nichyparenka.agency.service.jpa;

import com.epam.nichyparenka.agency.configuration.JpaConfiguration;
import com.epam.nichyparenka.agency.entity.Country;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.service.CountryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JpaConfiguration.class})
@ActiveProfiles({"jpa", "test"})
public class CountryServiceJpaTest {
    private static final String COUNTRY_NAME_1 = "Canada";
    private static final String COUNTRY_NAME_2 = "Japan";
    private static final int ID_INCORRECT = -100;
    private Country country = new Country();

    @Autowired
    @Qualifier("countryService")
    private CountryService countryService;

    @Test
    public void testCountryCreateTrue() {
        country.setName(COUNTRY_NAME_1);
        assertTrue(countryService.createCountry(country));
    }

    @Test
    public void testCountryCreateFalse() {
        Country country = new Country();
        country.setId(ID_INCORRECT);
        country.setName(COUNTRY_NAME_1);
        try {
            countryService.createCountry(country);
            fail();
        } catch (Exception exception) {
            assertTrue(true);
        }
    }

    @Test
    public void testCountryReadTrue() {
        country.setName(COUNTRY_NAME_1);
        countryService.createCountry(country);
        Optional<Country> countryOptional = countryService.readCountry(country.getId());
        if (countryOptional.isPresent()) {
            assertEquals(COUNTRY_NAME_1, countryOptional.get().getName());
        } else {
            fail();
        }
    }

    @Test
    public void testCountryReadFalse() {
        Optional<Country> countryOptional = countryService.readCountry(ID_INCORRECT);
        if (countryOptional.isPresent()) {
            fail();
        } else {
            assertTrue(true);
        }
    }

    @Test
    public void testCountryUpdateTrue() {
        country.setName(COUNTRY_NAME_1);
        countryService.createCountry(country);
        country.setName(COUNTRY_NAME_2);
        boolean result = countryService.updateCountry(country);
        if (result) {
            Optional<Country> countryOptional = countryService.readCountry(country.getId());
            if (countryOptional.isPresent()) {
                assertEquals(COUNTRY_NAME_2, countryOptional.get().getName());
            } else {
                fail();
            }
        } else {
            fail();
        }
    }

    @Test
    public void testCountryUpdateFalse() {
        country.setName(COUNTRY_NAME_1);
        countryService.createCountry(country);
        country.setId(ID_INCORRECT);
        boolean result = countryService.updateCountry(country);
        if (result) {
            fail();
        } else {
            assertTrue(true);
        }
    }

    @Test
    public void testCountryDeleteTrue() {
        country.setName(COUNTRY_NAME_1);
        countryService.createCountry(country);
        boolean result = countryService.deleteCountry(country);
        if (result) {
            Optional<Country> countryOptional = countryService.readCountry(country.getId());
            if (countryOptional.isPresent()) {
                fail();
            } else {
                assertTrue(true);
            }
        } else {
            fail();
        }
    }

    @Test
    public void testCountryDeleteFalse() {
        Country country = new Country();
        country.setId(ID_INCORRECT);
        country.setName(COUNTRY_NAME_1);
        assertFalse(countryService.deleteCountry(country));
    }

    @Test
    public void testCountriesDeleteTrue() {
        Country country2 = new Country();
        country2.setName(COUNTRY_NAME_1);

        Country country3 = new Country();
        country3.setName(COUNTRY_NAME_2);

        countryService.createCountry(country2);
        countryService.createCountry(country3);

        try {
            countryService.deleteCountries(country2, country3);
            assertTrue(true);
        } catch (BadQueryExecutionException exception) {
            fail();
        }
    }

    @Test
    public void testCountriesDeleteFalse() {
        Country country = new Country();
        country.setId(ID_INCORRECT);
        country.setName(COUNTRY_NAME_1);

        Country country2 = new Country();
        country2.setId(ID_INCORRECT);
        country2.setName(COUNTRY_NAME_2);

        try {
            countryService.deleteCountries(country, country2);
            fail();
        } catch (BadQueryExecutionException exception) {
            assertTrue(true);
        }
    }
}
