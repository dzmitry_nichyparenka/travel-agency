package com.epam.nichyparenka.agency.service.postgres;

import com.epam.nichyparenka.agency.configuration.PostgresConfiguration;
import com.epam.nichyparenka.agency.entity.User;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.service.UserService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PostgresConfiguration.class})
@ActiveProfiles({"postgres", "test"})
public class UserServicePostgresTest {
    private static final String USER_LOGIN_1 = "excelsiorious";
    private static final String USER_LOGIN_2 = "shipilev123";
    private static final String USER_PASSWORD_1 = "7c6a180b36896a0a8c02787eeafb0e4c";
    private static final String USER_PASSWORD_2 = "1a79a4d60de6718e8e5b326e338ae533";
    private static final String USER_EMAIL_1 = "excelsiorious@gmail.com";
    private static final String USER_EMAIL_2 = "example_shipilev@gmail.com";
    private static final String USER_FIRST_NAME_1 = "Dmitry";
    private static final String USER_FIRST_NAME_2 = "Alexey";
    private static final String USER_LAST_NAME_1 = "Nichiporenko";
    private static final String USER_LAST_NAME_2 = "Shipilev";
    private static final int ID_INCORRECT = -100;
    private static User user = new User();

    @Autowired
    @Qualifier("userService")
    private UserService userService;

    @BeforeClass
    public static void beforeClass() {
        user.setId(4);
        user.setLogin(USER_LOGIN_1);
        user.setPassword(USER_PASSWORD_1);
        user.setEmail(USER_EMAIL_1);
        user.setFirstName(USER_FIRST_NAME_1);
        user.setLastName(USER_LAST_NAME_1);
    }

    @Test
    public void testUserCreateTrue() {
        assertTrue(userService.createUser(user));
    }

    @Test
    public void testUserCreateFalse() {
        User user2 = new User();
        user2.setId(ID_INCORRECT);
        assertFalse(userService.createUser(user2));
    }

    @Test
    public void testUserReadTrue() {
        userService.createUser(user);
        Optional<User> userOptional = userService.readUser(user.getId());
        if (userOptional.isPresent()) {
            User anotherUser = userOptional.get();
            assertEquals(user, anotherUser);
        } else {
            fail();
        }
    }

    @Test
    public void testUserReadFalse() {
        Optional<User> userOptional = userService.readUser(ID_INCORRECT);
        if (userOptional.isPresent()) {
            fail();
        } else {
            assertNotEquals(null, user);
        }
    }

    @Test
    public void testUserUpdateTrue() {
        userService.createUser(user);

        User user2 = new User();
        user2.setId(1);
        user2.setLogin(USER_LOGIN_2);
        user2.setPassword(USER_PASSWORD_2);
        user2.setEmail(USER_EMAIL_2);
        user2.setFirstName(USER_FIRST_NAME_2);
        user2.setLastName(USER_LAST_NAME_2);

        assertTrue(userService.updateUser(user2));
    }

    @Test
    public void testUserUpdateFalse() {
        User user2 = new User();
        user2.setId(ID_INCORRECT);

        assertFalse(userService.updateUser(user2));
    }

    @Test
    public void testUserDeleteTrue() {
        userService.createUser(user);
        assertTrue(userService.deleteUser(user));
    }

    @Test
    public void testUserDeleteFalse() {
        User user = new User();
        user.setId(ID_INCORRECT);

        assertFalse(userService.deleteUser(user));
    }

    @Test
    public void testUsersDeleteTrue() {
        long id = user.getId();
        userService.createUser(user);
        userService.createUser(user);
        userService.createUser(user);
        user.setId(5);
        User user2 = new User();
        user2.setId(6);
        try {
            userService.deleteUsers(user, user2);
            user.setId(id);
            assertTrue(true);
        } catch (BadQueryExecutionException exception) {
            user.setId(id);
            fail();
        }
    }

    @Test
    public void testUsersDeleteFalse() {
        User user = new User();
        user.setId(ID_INCORRECT);
        User user2 = new User();
        user2.setId(ID_INCORRECT);
        try {
            userService.deleteUsers(user, user2);
            fail();
        } catch (BadQueryExecutionException exception) {
            assertTrue(true);
        }
    }
}