package com.epam.nichyparenka.agency.service.postgres;

import com.epam.nichyparenka.agency.configuration.PostgresConfiguration;
import com.epam.nichyparenka.agency.entity.*;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.service.TourService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PostgresConfiguration.class})
@ActiveProfiles({"postgres", "test"})
public class TourServicePostgresTest {
    private static final String TOUR_NAME_1 = "Excursion tour to China";
    private static final String TOUR_NAME_2 = "Test name";
    private static final String TOUR_DESCRIPTION_1 = "Excursion tour to China";
    private static final int ID_INCORRECT = -100;
    private static Tour tour = new Tour();
    private static User user = new User();

    @Autowired
    @Qualifier("tourService")
    private TourService tourService;

    @BeforeClass
    public static void beforeClass() {
        LocalDate localDate = LocalDate.of(2018, 4, 15);
        Country country = new Country();
        Hotel hotel = new Hotel();
        country.setId(1);
        hotel.setId(1);

        tour.setId(4);
        tour.setCountry(country);
        tour.setHotel(hotel);
        tour.setName(TOUR_NAME_1);
        tour.setDescription(TOUR_DESCRIPTION_1);
        tour.setDate(localDate);
        tour.setCost(BigDecimal.valueOf(950.50).setScale(2, BigDecimal.ROUND_HALF_UP));
        tour.setDuration(Period.ofDays(5));
        tour.setType(TourType.EXCURSION);

        user.setId(1);
    }

    @Test
    public void testTourCreateTrue() {
        try {
            tour.setId(9);
            boolean actual = tourService.createTour(tour, user);
            tour.setId(4);
            assertTrue(actual);
        } catch (BadQueryExecutionException exception) {
            tour.setId(4);
            fail();
        }
    }

    @Test
    public void testTourCreateFalse() {
        tour.setId(ID_INCORRECT);
        try {
            assertFalse(tourService.createTour(tour, user));
        } catch (BadQueryExecutionException exception) {
            assertTrue(true);
        }
    }

    @Test
    public void testTourReadTrue() {
        try {
            tourService.createTour(tour, user);
        } catch (BadQueryExecutionException exception) {
            fail();
        }
        Optional<Tour> tourOptional = tourService.readTour(4);
        if (tourOptional.isPresent()) {
            Tour anotherTour = tourOptional.get();
            assertEquals(tour, anotherTour);
        } else {
            fail();
        }
    }

    @Test
    public void testTourReadFalse() {
        Optional<Tour> tourOptional = tourService.readTour(ID_INCORRECT);
        if (tourOptional.isPresent()) {
            fail();
        } else {
            assertNotEquals(null, tour);
        }
    }

    @Test
    public void testTourUpdateTrue() {
        try {
            tourService.createTour(tour, user);
        } catch (BadQueryExecutionException exception) {
            fail();
        }
        tour.setName(TOUR_NAME_2);
        assertTrue(tourService.updateTour(tour));
    }

    @Test
    public void testTourUpdateFalse() {
        tour.setId(ID_INCORRECT);
        assertFalse(tourService.updateTour(tour));
    }

    @Test
    public void testTourDeleteTrue() {
        tour.setId(5);
        try {
            tourService.createTour(tour, user);
        } catch (BadQueryExecutionException exception) {
            fail();
        }
        assertTrue(tourService.deleteTour(tour));
    }

    @Test
    public void testTourDeleteFalse() {
        Tour tour = new Tour();
        tour.setId(ID_INCORRECT);
        assertFalse(tourService.deleteTour(tour));
    }

    @Test
    public void testToursDeleteTrue() {
        long id = tour.getId();
        try {
            tourService.createTour(tour, user);
            tourService.createTour(tour, user);
            tourService.createTour(tour, user);
            tour.setId(6);
            Tour tour2 = new Tour();
            tour2.setId(4);
            tourService.deleteTours(tour, tour2);
            tour.setId(id);
            assertTrue(true);
        } catch (BadQueryExecutionException exception) {
            tour.setId(id);
            fail();
        }
    }

    @Test
    public void testToursDeleteFalse() {
        Tour tour = new Tour();
        tour.setId(ID_INCORRECT);
        Tour tour2 = new Tour();
        tour2.setId(ID_INCORRECT);
        try {
            tourService.deleteTours(tour, tour2);
            fail();
        } catch (BadQueryExecutionException exception) {
            assertTrue(true);
        }
    }
}