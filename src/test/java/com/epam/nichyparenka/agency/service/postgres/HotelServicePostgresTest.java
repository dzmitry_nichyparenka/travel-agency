package com.epam.nichyparenka.agency.service.postgres;

import com.epam.nichyparenka.agency.configuration.PostgresConfiguration;
import com.epam.nichyparenka.agency.entity.Country;
import com.epam.nichyparenka.agency.entity.Hotel;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.service.HotelService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PostgresConfiguration.class})
@ActiveProfiles({"postgres", "test"})
public class HotelServicePostgresTest {
    private static final String HOTEL_NAME_1 = "Example hotel";
    private static final String HOTEL_NAME_2 = "Holiday Inn Totowa Wayne";
    private static final String HOTEL_PHONE_NUMBER_1 = "+375 33 3638375";
    private static final int ID_INCORRECT = -100;
    private static Hotel hotel = new Hotel();
    private static Country country = new Country();

    @Autowired
    @Qualifier("hotelService")
    private HotelService hotelService;

    @BeforeClass
    public static void beforeClass() {
        country.setId(1);
        hotel.setId(7);
        hotel.setCountry(country);
        hotel.setName(HOTEL_NAME_1);
        hotel.setPhoneNumber(HOTEL_PHONE_NUMBER_1);
        hotel.setNumberOfStars((byte) 5);
    }

    @Test
    public void testHotelCreateTrue() {
        assertTrue(hotelService.createHotel(hotel));
    }

    @Test
    public void testHotelCreateFalse() {
        Country country = new Country();
        country.setId(1);
        Hotel hotel2 = new Hotel();
        hotel2.setId(ID_INCORRECT);
        hotel2.setCountry(country);
        hotel2.setName(HOTEL_NAME_1);
        hotel2.setNumberOfStars((byte) 1);
        hotel2.setPhoneNumber(HOTEL_PHONE_NUMBER_1);

        assertFalse(hotelService.createHotel(hotel2));
    }

    @Test
    public void testHotelReadTrue() {
        Optional<Hotel> hotelOptional = hotelService.readHotel(2);
        if (hotelOptional.isPresent()) {
            Hotel hotel = hotelOptional.get();
            assertEquals(HOTEL_NAME_2, hotel.getName());
        } else {
            fail();
        }
    }

    @Test
    public void testHotelReadFalse() {
        hotelService.createHotel(hotel);
        Optional<Hotel> hotelOptional = hotelService.readHotel(ID_INCORRECT);
        if (hotelOptional.isPresent()) {
            fail();
        } else {
            assertNotEquals(null, hotel);
        }
    }

    @Test
    public void testHotelUpdateTrue() {
        hotel.setCountry(country);
        hotelService.createHotel(hotel);
        Hotel hotel2 = new Hotel();
        hotel2.setId(1);
        hotel2.setCountry(country);
        hotel2.setName(HOTEL_NAME_1);
        hotel2.setPhoneNumber(HOTEL_PHONE_NUMBER_1);
        hotel2.setNumberOfStars((byte) 3);

        assertTrue(hotelService.updateHotel(hotel2));
    }

    @Test
    public void testHotelUpdateFalse() {
        Hotel hotel2 = new Hotel();
        hotel2.setId(2);
        hotel2.setCountry(country);
        assertFalse(hotelService.updateHotel(hotel2));
    }

    @Test
    public void testHotelDeleteTrue() {
        hotel.setId(5);
        assertTrue(hotelService.deleteHotel(hotel));
    }

    @Test
    public void testHotelDeleteFalse() {
        Hotel hotel = new Hotel();
        hotel.setId(ID_INCORRECT);
        assertFalse(hotelService.deleteHotel(hotel));
    }

    @Test
    public void testHotelsDeleteTrue() {
        Hotel hotel = new Hotel();
        hotel.setId(6);
        hotel.setCountry(country);
        Hotel hotel2 = new Hotel();
        hotel2.setId(7);
        hotel2.setCountry(country);
        hotelService.createHotel(hotel);
        hotelService.createHotel(hotel2);
        try {
            hotelService.deleteHotels(hotel, hotel2);
            assertTrue(true);
        } catch (BadQueryExecutionException exception) {
            fail();
        }
    }

    @Test
    public void testHotelsDeleteFalse() {
        Hotel hotel = new Hotel();
        hotel.setId(ID_INCORRECT);
        Hotel hotel2 = new Hotel();
        hotel2.setId(ID_INCORRECT);
        try {
            hotelService.deleteHotels(hotel, hotel2);
            fail();
        } catch (BadQueryExecutionException exception) {
            assertTrue(true);
        }
    }
}