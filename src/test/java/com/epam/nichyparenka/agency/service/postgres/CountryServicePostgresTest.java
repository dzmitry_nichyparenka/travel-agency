package com.epam.nichyparenka.agency.service.postgres;

import com.epam.nichyparenka.agency.configuration.PostgresConfiguration;
import com.epam.nichyparenka.agency.entity.Country;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.service.CountryService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PostgresConfiguration.class})
@ActiveProfiles({"postgres", "test"})
public class CountryServicePostgresTest {
    private static final String COUNTRY_NAME_1 = "Canada";
    private static final String COUNTRY_NAME_2 = "Japan";
    private static final int ID_INCORRECT = -100;
    private static Country country = new Country();

    @Autowired
    @Qualifier("countryService")
    private CountryService countryService;

    @BeforeClass
    public static void beforeClass() {
        country.setId(6);
        country.setName(COUNTRY_NAME_1);
    }

    @Test
    public void testCountryCreateTrue() {
        assertTrue(countryService.createCountry(country));
    }

    @Test
    public void testCountryCreateFalse() {
        Country country2 = new Country();
        country2.setId(ID_INCORRECT);
        country2.setName(COUNTRY_NAME_2);
        assertFalse(countryService.createCountry(country2));
    }

    @Test
    public void testCountryReadTrue() {
        Optional<Country> countryOptional = countryService.readCountry(1);
        if (countryOptional.isPresent()) {
            Country anotherCountry = countryOptional.get();
            assertEquals(COUNTRY_NAME_1, anotherCountry.getName());
        } else {
            fail();
        }
    }

    @Test
    public void testCountryReadFalse() {
        Optional<Country> countryOptional = countryService.readCountry(ID_INCORRECT);
        if (countryOptional.isPresent()) {
            fail();
        } else {
            assertNotEquals(null, country);
        }
    }

    @Test
    public void testCountryUpdateTrue() {
        Country country2 = new Country();
        country2.setId(1);
        country2.setName(COUNTRY_NAME_1);

        assertTrue(countryService.updateCountry(country2));
    }

    @Test
    public void testCountryUpdateFalse() {
        Country newCountry = new Country();
        newCountry.setId(ID_INCORRECT);
        newCountry.setName(COUNTRY_NAME_2);

        assertFalse(countryService.updateCountry(newCountry));
    }

    @Test
    public void testCountryDeleteTrue() {
        country.setId(5);
        assertTrue(countryService.deleteCountry(country));
    }

    @Test
    public void testCountryDeleteFalse() {
        Country country = new Country();
        country.setId(ID_INCORRECT);

        assertFalse(countryService.deleteCountry(country));
    }

    @Test
    public void testCountriesDeleteTrue() {
        countryService.createCountry(country);
        countryService.createCountry(country);
        countryService.createCountry(country);
        Country country = new Country();
        country.setId(7);
        Country country2 = new Country();
        country2.setId(8);
        try {
            countryService.deleteCountries(country, country2);
            assertTrue(true);
        } catch (BadQueryExecutionException exception) {
            fail();
        }
    }

    @Test
    public void testCountriesDeleteFalse() {
        Country country = new Country();
        country.setId(ID_INCORRECT);
        Country country2 = new Country();
        country.setId(ID_INCORRECT);
        try {
            countryService.deleteCountries(country, country2);
            fail();
        } catch (BadQueryExecutionException e) {
            assertTrue(true);
        }
    }
}