package com.epam.nichyparenka.agency.service.jpa;

import com.epam.nichyparenka.agency.configuration.JpaConfiguration;
import com.epam.nichyparenka.agency.entity.*;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.service.CountryService;
import com.epam.nichyparenka.agency.service.HotelService;
import com.epam.nichyparenka.agency.service.TourService;
import com.epam.nichyparenka.agency.service.UserService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JpaConfiguration.class})
@ActiveProfiles({"jpa", "test"})
public class TourServiceJpaTest {
    private static final String COUNTRY_NAME = "Canada";
    private static final String HOTEL_NAME = "Example hotel";
    private static final String HOTEL_PHONE_NUMBER = "+375 33 3638375";
    private static final byte HOTEL_STARS_COUNT = 4;
    private static final String USER_LOGIN = "excelsiorious";
    private static final String USER_PASSWORD = "7c6a180b36896a0a8c02787eeafb0e4c";
    private static final String USER_EMAIL = "excelsiorious@gmail.com";
    private static final String USER_FIRST_NAME = "Dmitry";
    private static final String USER_LAST_NAME = "Nichiporenko";
    private static final String TOUR_NAME_1 = "Excursion tour to China";
    private static final String TOUR_NAME_2 = "Business tour to USA";
    private static final String TOUR_BAD_NAME = "0";
    private static final String TOUR_DESCRIPTION = "Excursion tour to China";
    private static final int ID_INCORRECT = -100;
    private static Country country = new Country();
    private static Hotel hotel = new Hotel();
    private static User user = new User();
    private static Tour tour = new Tour();

    @Autowired
    @Qualifier("countryService")
    private CountryService countryService;

    @Autowired
    @Qualifier("hotelService")
    private HotelService hotelService;

    @Autowired
    @Qualifier("userService")
    private UserService userService;

    @Autowired
    @Qualifier("tourService")
    private TourService tourService;

    @BeforeClass
    public static void beforeClass() {
        country.setName(COUNTRY_NAME);

        hotel.setName(HOTEL_NAME);
        hotel.setCountry(country);
        hotel.setPhoneNumber(HOTEL_PHONE_NUMBER);
        hotel.setNumberOfStars(HOTEL_STARS_COUNT);

        user.setLogin(USER_LOGIN);
        user.setPassword(USER_PASSWORD);
        user.setEmail(USER_EMAIL);
        user.setFirstName(USER_FIRST_NAME);
        user.setLastName(USER_LAST_NAME);

        LocalDate localDate = LocalDate.of(2018, 4, 15);
        tour.setCountry(country);
        tour.setHotel(hotel);
        tour.setName(TOUR_NAME_1);
        tour.setDescription(TOUR_DESCRIPTION);
        tour.setDate(localDate);
        tour.setCost(BigDecimal.valueOf(950.50).setScale(2, BigDecimal.ROUND_HALF_UP));
        tour.setDuration(Period.ofDays(5));
        tour.setType(TourType.EXCURSION);
    }

    @Test
    public void testTourCreateTrue() {
        resetIds();
        countryService.createCountry(country);
        hotelService.createHotel(hotel);
        userService.createUser(user);
        try {
            assertTrue(tourService.createTour(tour, user));
        } catch (BadQueryExecutionException exception) {
            fail();
        }
    }

    @Test
    public void testTourCreateFalse() {
        resetIds();
        countryService.createCountry(country);
        hotelService.createHotel(hotel);
        userService.createUser(user);
        try {
            tour.setId(ID_INCORRECT);
            tourService.createTour(tour, user);
            fail();
        } catch (Exception exception) {
            assertTrue(true);
        }
    }

    @Test
    public void testTourReadTrue() {
        try {
            resetIds();
            countryService.createCountry(country);
            hotelService.createHotel(hotel);
            userService.createUser(user);
            tourService.createTour(tour, user);
            Optional<Tour> tourOptional = tourService.readTour(tour.getId());
            if (tourOptional.isPresent()) {
                Tour tour = tourOptional.get();
                assertTrue(tour.getCountry().getName().length() > 0);
            }
        } catch (BadQueryExecutionException exception) {
            fail();
        }
    }

    @Test
    public void testTourReadFalse() {
        Optional<Tour> tourOptional = tourService.readTour(ID_INCORRECT);
        if (tourOptional.isPresent()) {
            fail();
        } else {
            assertTrue(true);
        }
    }

    @Test
    public void testTourUpdateTrue() {
        countryService.createCountry(country);
        hotelService.createHotel(hotel);
        userService.createUser(user);
        try {
            tourService.createTour(tour, user);
            tour.setName(TOUR_NAME_2);
            boolean result = tourService.updateTour(tour);
            if (result) {
                Optional<Tour> tourOptional = tourService.readTour(tour.getId());
                if (tourOptional.isPresent()) {
                    assertEquals(tour.getName(), tourOptional.get().getName());
                } else {
                    fail();
                }
            } else {
                fail();
            }
        } catch (BadQueryExecutionException exception) {
            fail();
        }
    }

    @Test
    public void testTourUpdateFalse() {
        resetIds();
        countryService.createCountry(country);
        hotelService.createHotel(hotel);
        userService.createUser(user);
        try {
            tourService.createTour(tour, user);
            tour.setName(TOUR_BAD_NAME);
            boolean result = tourService.updateTour(tour);
            if (result) {
                fail();
            }
        } catch (Exception exception) {
            assertTrue(true);
        }
    }

    @Test
    public void testTourDeleteTrue() {
        boolean result = tourService.deleteTour(tour);
        if (result) {
            Optional<Tour> tourOptional = tourService.readTour(tour.getId());
            if (tourOptional.isPresent()) {
                fail();
            } else {
                assertTrue(true);
            }
        } else {
            fail();
        }
    }

    @Test
    public void testTourDeleteFalse() {
        Tour tour = new Tour();
        LocalDate localDate = LocalDate.of(2018, 4, 15);
        tour.setCountry(country);
        tour.setHotel(hotel);
        tour.setId(ID_INCORRECT);
        tour.setName(TOUR_NAME_1);
        tour.setDescription(TOUR_DESCRIPTION);
        tour.setDate(localDate);
        tour.setCost(BigDecimal.valueOf(950.50).setScale(2, BigDecimal.ROUND_HALF_UP));
        tour.setDuration(Period.ofDays(5));
        tour.setType(TourType.EXCURSION);
        assertFalse(tourService.deleteTour(tour));
    }

    @Test
    public void testToursDeleteTrue() {
        LocalDate localDate = LocalDate.of(2018, 4, 15);
        Tour tour = new Tour();
        tour.setCountry(country);
        tour.setHotel(hotel);
        tour.setName(TOUR_NAME_1);
        tour.setDescription(TOUR_DESCRIPTION);
        tour.setDate(localDate);
        tour.setCost(BigDecimal.valueOf(950.50).setScale(2, BigDecimal.ROUND_HALF_UP));
        tour.setDuration(Period.ofDays(5));
        tour.setType(TourType.EXCURSION);

        Tour tour2 = new Tour();
        tour2.setCountry(country);
        tour2.setHotel(hotel);
        tour2.setName(TOUR_NAME_1);
        tour2.setDescription(TOUR_DESCRIPTION);
        tour2.setDate(localDate);
        tour2.setCost(BigDecimal.valueOf(950.50).setScale(2, BigDecimal.ROUND_HALF_UP));
        tour2.setDuration(Period.ofDays(5));
        tour2.setType(TourType.EXCURSION);

        try {
            tourService.createTour(tour, user);
            tourService.createTour(tour2, user);
            tourService.deleteTours(tour, tour2);
        } catch (BadQueryExecutionException exception) {
            fail();
        }
    }

    @Test
    public void testToursDeleteFalse() {
        LocalDate localDate = LocalDate.of(2018, 4, 15);
        Tour tour = new Tour();
        tour.setCountry(country);
        tour.setHotel(hotel);
        tour.setName(TOUR_NAME_1);
        tour.setDescription(TOUR_DESCRIPTION);
        tour.setDate(localDate);
        tour.setCost(BigDecimal.valueOf(950.50).setScale(2, BigDecimal.ROUND_HALF_UP));
        tour.setDuration(Period.ofDays(5));
        tour.setType(TourType.EXCURSION);

        Tour tour2 = new Tour();
        tour2.setCountry(country);
        tour2.setHotel(hotel);
        tour2.setName(TOUR_NAME_1);
        tour2.setDescription(TOUR_DESCRIPTION);
        tour2.setDate(localDate);
        tour2.setCost(BigDecimal.valueOf(950.50).setScale(2, BigDecimal.ROUND_HALF_UP));
        tour2.setDuration(Period.ofDays(5));
        tour2.setType(TourType.EXCURSION);

        try {
            tourService.createTour(tour, user);
            tourService.createTour(tour2, user);

            tour.setId(ID_INCORRECT);
            tour2.setId(ID_INCORRECT);

            tourService.deleteTours(tour, tour2);
            fail();
        } catch (BadQueryExecutionException exception) {
            assertTrue(true);
        }
    }

    private void resetIds() {
        country.setId(0);
        hotel.setId(0);
        user.setId(0);
        tour.setId(0);
    }
}
