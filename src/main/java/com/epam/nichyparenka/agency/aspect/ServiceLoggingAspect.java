package com.epam.nichyparenka.agency.aspect;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class provides aspect for logging of all
 * methods in service classes.
 */
@Aspect
public class ServiceLoggingAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceLoggingAspect.class);

    /**
     * Logging of unfinished methods (an exception
     * has been thrown at runtime).
     */
    @AfterThrowing("execution(* com.epam.nichyparenka.agency.service.*Service.delete*s(..))")
    public void afterThrowingAdvice() {
        LOGGER.info("Something went wrong. An exceptions has been thrown.");
    }
}
