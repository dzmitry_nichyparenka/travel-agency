package com.epam.nichyparenka.agency.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class provides aspect for logging of all
 * methods in repository classes.
 */
@Aspect
public class RepositoryLoggingAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryLoggingAspect.class);
    private static final String SPLITTER_CLASS_NAME = "\\$\\$";

    /**
     * Pointcut for all methods in classes, annotated
     * as `Repository`.
     */
    @Pointcut("@within(org.springframework.stereotype.Repository))")
    private void annotatedWithRepository() {}

    /**
     * Logging of successfully finished or unfinished
     * (an exception has been thrown at runtime) methods.
     *
     * @param joinPoint join point for this advice
     * @return result of the execution
     */
    @Around("annotatedWithRepository()")
    public Object aroundAdvice(ProceedingJoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        String className = joinPoint.getThis().getClass().getSimpleName().split(SPLITTER_CLASS_NAME)[0];
        try {
            Object result = joinPoint.proceed();
            if (result instanceof Boolean) {
                if ((boolean) result) {
                    LOGGER.info("Method `" + methodName + "` in repository class `" + className + "` has been successfully executed.");
                } else {
                    LOGGER.info("Method `" + methodName + "` in repository class `" + className + "` has been executed with false result.");
                }
            }
            return result;
        } catch (Throwable throwable) {
            LOGGER.info("Method `" + methodName + "` in repository class `" + className + "` hasn't been executed normally. " +
                    "An exception has been thrown.", throwable);
            return null;
        }
    }
}
