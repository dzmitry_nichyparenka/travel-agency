package com.epam.nichyparenka.agency.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

/**
 * The class provides storage of data about tours. Class
 * extends {@link BasicEntity} and inherits id from it.
 *
 * @author Dzmitry Nichyparenka
 */
@Entity(name = "Tour")
@Table(name = "tours")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@OptimisticLocking(type = OptimisticLockType.VERSION)
public class Tour extends BasicEntity implements Serializable {
    @Version
    private int version;

    @NotNull
    @Size(min = 10, max = 50)
    @Column(name = "name")
    private String name;

    @NotNull
    @Size(min = 10, max = 150)
    @Column(name = "description")
    private String description;

    @Column(name = "photo")
    private byte[] photo;

    @NotNull
    @Column(name = "start_date")
    private LocalDate date;

    @NotNull
    @Column(name = "duration")
    private Period duration;

    @NotNull
    @ManyToOne
    private Country country;

    @ManyToOne
    private Hotel hotel;

    @NotNull
    @Column(name = "type")
    private TourType type;

    @NotNull
    @Min(1)
    @Column(name = "cost")
    private BigDecimal cost;
}
