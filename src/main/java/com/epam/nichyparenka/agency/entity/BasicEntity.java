package com.epam.nichyparenka.agency.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;

/**
 * The class provides abstract entity for all entities in
 * project. Each new entity must be extended from this class.
 *
 * @author Dzmitry Nichyparenka
 */
@Data
@MappedSuperclass
public abstract class BasicEntity implements Serializable {
    /**
     * The unique identifier of the specified entity.
     */
    @Id
    @NotNull
    @PositiveOrZero
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected long id;
}
