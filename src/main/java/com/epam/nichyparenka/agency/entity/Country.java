package com.epam.nichyparenka.agency.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The class provides storage of data about countries. Class
 * extends {@link BasicEntity} and inherits id from it.
 *
 * @author Dzmitry Nichyparenka
 */
@NamedNativeQueries({
        @NamedNativeQuery(name = "selectAllCountries", query = "SELECT * FROM Countries", resultClass = Country.class)
})
@Entity(name = "Country")
@Table(name = "countries")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@OptimisticLocking(type = OptimisticLockType.VERSION)
public class Country extends BasicEntity implements Serializable {
    @Version
    private int version;

    @NotNull
    @Size(min = 3, max = 30)
    @Column(name = "name")
    private String name;
}
