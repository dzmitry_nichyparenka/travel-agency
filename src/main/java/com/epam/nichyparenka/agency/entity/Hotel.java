package com.epam.nichyparenka.agency.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The class provides storage of data about hotels. Class
 * extends {@link BasicEntity} and inherits id from it.
 *
 * @author Dzmitry Nichyparenka
 */
@NamedNativeQueries({
        @NamedNativeQuery(name = "selectAllHotels", query = "SELECT * FROM Hotels", resultClass = Hotel.class)
})
@Entity(name = "Hotel")
@Table(name = "hotels")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@OptimisticLocking(type = OptimisticLockType.VERSION)
public class Hotel extends BasicEntity implements Serializable {
    @Version
    private int version;

    @NotNull
    @ManyToOne(targetEntity = Country.class, fetch = FetchType.EAGER)
    private Country country;

    @NotNull
    @Length(min = 3, max = 50)
    @Column(name = "name")
    private String name;

    @NotNull
    @Size(min = 3, max = 30)
    @Column(name = "phone")
    private String phoneNumber;

    @NotNull
    @Min(1)
    @Max(5)
    @Column(name = "stars")
    private byte numberOfStars;
}
