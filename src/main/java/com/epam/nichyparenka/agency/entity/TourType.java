package com.epam.nichyparenka.agency.entity;

/**
 * This enum provides different tour types.
 *
 * @author Dzmitry Nichyparenka
 */
public enum TourType {
    RECREATION, EXCURSION, SPORTS, HEALTH, BUSINESS;
}
