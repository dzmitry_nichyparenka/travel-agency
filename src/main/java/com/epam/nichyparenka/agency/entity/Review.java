package com.epam.nichyparenka.agency.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The class provides storage of data about reviews. Class
 * extends {@link BasicEntity} and inherits id from it.
 *
 * @author Dzmitry Nichyparenka
 */
@Entity(name = "Review")
@Table(name = "reviews")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@OptimisticLocking(type = OptimisticLockType.VERSION)
public class Review extends BasicEntity implements Serializable {
    @Version
    private int version;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "tour_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Tour tour;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
    private User user;

    @Size(min = 30, max = 250)
    @Column(name = "text")
    private String text;
}
