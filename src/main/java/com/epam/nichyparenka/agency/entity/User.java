package com.epam.nichyparenka.agency.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * The class provides storage of data about users. Class
 * extends {@link BasicEntity} and inherits id from it.
 *
 * @author Dzmitry Nichyparenka
 */
@Entity(name = "User")
@Table(name = "users")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true, exclude = {"password"})
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@OptimisticLocking(type = OptimisticLockType.VERSION)
public class User extends BasicEntity implements Serializable {
    @Version
    private int version;

    @NotNull
    @Size(min = 5, max = 50)
    @Column(name = "login")
    private String login;

    @NotNull
    @Size(min = 10, max = 50)
    @Column(name = "password")
    private String password;

    @NotNull
    @Size(min = 10, max = 50)
    @Column(name = "email")
    private String email;

    @NotNull
    @Size(min = 3, max = 30)
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Size(min = 3, max = 30)
    @Column(name = "last_name")
    private String lastName;
}
