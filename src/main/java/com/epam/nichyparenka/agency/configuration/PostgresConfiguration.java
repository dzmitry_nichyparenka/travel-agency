package com.epam.nichyparenka.agency.configuration;

import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * The class provides configuration for all beans in the
 * program which are associated with the Postgres profile.
 * This configuration can be used for production and for
 * testing purposes.
 *
 * @author Dzmitry Nichyparenka
 */
@Profile("postgres")
@Configuration
@EnableTransactionManagement
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan("com.epam.nichyparenka.agency")
public abstract class PostgresConfiguration extends BasicConfiguration {
    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }
}
