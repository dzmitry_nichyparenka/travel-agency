package com.epam.nichyparenka.agency.configuration;

import com.epam.nichyparenka.agency.aspect.RepositoryLoggingAspect;
import com.epam.nichyparenka.agency.aspect.ServiceLoggingAspect;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;
import java.util.Collections;

/**
 * The class provides common beans and fields (it also can
 * include all, which can be used in all configurations).
 * By itself, it can not be used as a configuration. Every
 * configuration must extend this class.
 *
 * @author Dzmitry Nichyparenka
 */
@EnableCaching
@PropertySource("classpath:database/database.properties")
abstract class BasicConfiguration {
    private static final String PROPERTY_SQL_SCHEMA = "sql.schema";
    private static final String PROPERTY_SQL_INIT_DATA = "sql.init.data";
    private static final String PROPERTY_POSTGRES_URL = "database.url";
    private static final String PROPERTY_POOL_MAX_SIZE = "connection.pool.size";
    private static final String PROPERTY_POSTGRES_USERNAME = "postgres.username";
    private static final String PROPERTY_POSTGRES_PASSWORD = "postgres.password";
    private static final int DEFAULT_POOL_SIZE = 15;

    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(Collections.singletonList(new ConcurrentMapCache("default")));
        return cacheManager;
    }

    @Autowired
    Environment environment;

    @Bean
    RepositoryLoggingAspect repositoryLoggingAspect() {
        return new RepositoryLoggingAspect();
    }

    @Bean
    ServiceLoggingAspect serviceLoggingAspect() {
        return new ServiceLoggingAspect();
    }

    @Profile("prod")
    @Bean(name = "dataSource")
    DataSource dataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(environment.getProperty(PROPERTY_POSTGRES_URL));
        dataSource.setUsername(environment.getProperty(PROPERTY_POSTGRES_USERNAME));
        dataSource.setPassword(environment.getProperty(PROPERTY_POSTGRES_PASSWORD));
        try {
            dataSource.setMaximumPoolSize(Integer.parseInt(environment.getProperty(PROPERTY_POOL_MAX_SIZE)));
        } catch (NumberFormatException exception) {
            dataSource.setMaximumPoolSize(DEFAULT_POOL_SIZE);
        }
        dataSource.setSchema(environment.getProperty(PROPERTY_SQL_SCHEMA));
        return dataSource;
    }

    @Profile("test")
    @Bean(name = "dataSource")
    DataSource dataSourceTest() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript(environment.getProperty(PROPERTY_SQL_SCHEMA))
                .addScript(environment.getProperty(PROPERTY_SQL_INIT_DATA))
                .build();
    }
}
