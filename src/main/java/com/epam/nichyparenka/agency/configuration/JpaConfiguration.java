package com.epam.nichyparenka.agency.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import java.util.Properties;

/**
 * The class provides configurations for all beans in the
 * program which are associated with the JPA profile. This
 * configuration can be used for production and for testing
 * purposes.
 *
 * @author Dzmitry Nichyparenka
 */
@Profile("jpa")
@Configuration
@EnableTransactionManagement
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan("com.epam.nichyparenka.agency")
public abstract class JpaConfiguration extends BasicConfiguration {
    private static final String PROPERTY_HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    private static final String PROPERTY_HIBERNATE_DIALECT = "hibernate.dialect";
    private static final String PROPERTY_HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";
    private static final String PROPERTY_HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
    private static final String PROPERTY_HIBERNATE_USE_SQL_COMMENTS = "hibernate.use_sql_comments";
    private static final String PROPERTY_HIBERNATE_USE_2ND_CACHE = "hibernate.cache.use_second_level_cache";
    private static final String PROPERTY_HIBERNATE_CACHE_FACTORY = "hibernate.cache.region.factory_class";
    private static final String PACKAGES_TO_SCAN = "com.epam.nichyparenka.agency";

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabase(Database.POSTGRESQL);
        vendorAdapter.setShowSql(Boolean.parseBoolean(environment.getProperty(PROPERTY_HIBERNATE_SHOW_SQL)));

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setPackagesToScan(PACKAGES_TO_SCAN);
        entityManagerFactoryBean.setJpaVendorAdapter(vendorAdapter);
        entityManagerFactoryBean.setJpaProperties(hibernateProperties());
        return entityManagerFactoryBean;
    }

    @Bean
    @Autowired
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    private Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty(PROPERTY_HIBERNATE_HBM2DDL_AUTO, environment.getProperty(PROPERTY_HIBERNATE_HBM2DDL_AUTO));
        hibernateProperties.setProperty(PROPERTY_HIBERNATE_DIALECT, environment.getProperty(PROPERTY_HIBERNATE_DIALECT));
        hibernateProperties.setProperty(PROPERTY_HIBERNATE_SHOW_SQL, environment.getProperty(PROPERTY_HIBERNATE_SHOW_SQL));
        hibernateProperties.setProperty(PROPERTY_HIBERNATE_FORMAT_SQL, environment.getProperty(PROPERTY_HIBERNATE_FORMAT_SQL));
        hibernateProperties.setProperty(PROPERTY_HIBERNATE_USE_SQL_COMMENTS, environment.getProperty(PROPERTY_HIBERNATE_USE_SQL_COMMENTS));
        hibernateProperties.setProperty(PROPERTY_HIBERNATE_USE_2ND_CACHE, environment.getProperty(PROPERTY_HIBERNATE_USE_2ND_CACHE));
        hibernateProperties.setProperty(PROPERTY_HIBERNATE_CACHE_FACTORY, environment.getProperty(PROPERTY_HIBERNATE_CACHE_FACTORY));
        return hibernateProperties;
    }
}
