package com.epam.nichyparenka.agency.exception;

/**
 * The class provides exception which used for
 * transactions.
 *
 * @author Dzmitry Nichyparenka
 */
public class BadQueryExecutionException extends Exception {
    public BadQueryExecutionException(String message) {
        super(message);
    }
}
