package com.epam.nichyparenka.agency.repository.specification.impl;

import com.epam.nichyparenka.agency.entity.Tour;
import com.epam.nichyparenka.agency.repository.specification.SqlSpecification;
import com.epam.nichyparenka.agency.repository.specification.TourSpecification;

/**
 * The class implements tour specification, SQL
 * specification and provides a tour search by id.
 *
 * @author Dzmitry Nichyparenka
 */
public class TourFindByIdSpecification implements TourSpecification, SqlSpecification {
    /**
     * The field contains unique identifier of the tour.
     */
    private int id;

    /**
     * This method creates tour specification object with id.
     *
     * @param id unique identifier of the tour
     */
    public TourFindByIdSpecification(int id) {
        this.id = id;
    }

    @Override
    public boolean specified(Tour tour) {
        return id == tour.getId();
    }

    @Override
    public String toSqlQuery() {
        return null;
    }
}
