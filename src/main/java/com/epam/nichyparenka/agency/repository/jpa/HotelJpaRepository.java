package com.epam.nichyparenka.agency.repository.jpa;

import com.epam.nichyparenka.agency.entity.Hotel;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

/**
 * The class provides implementation of JPA repository for
 * hotels. This class implements {@link BasicRepository}.
 *
 * @author Dzmitry Nichyparenka
 */
@Repository
@Profile({"!postgres"})
public class HotelJpaRepository implements BasicRepository<Hotel> {
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * This method creates the hotel in the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean create(Hotel hotel) {
        entityManager.joinTransaction();
        entityManager.persist(hotel);
        return true;
    }

    /**
     * This method reads the hotel data from the database
     * associated with current JPA implementation.
     */
    @Override
    public Optional<Hotel> read(long id) {
        return Optional.ofNullable(entityManager.find(Hotel.class, id));
    }

    /**
     * This method updates the hotel in the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean update(Hotel hotel) {
        entityManager.joinTransaction();
        Optional<Hotel> hotelOptional = Optional.ofNullable(entityManager.find(Hotel.class, hotel.getId()));
        if (hotelOptional.isPresent()) {
            Hotel hotelFound = hotelOptional.get();
            hotelFound.setName(hotel.getName());
            hotelFound.setPhoneNumber(hotel.getPhoneNumber());
            hotelFound.setNumberOfStars(hotel.getNumberOfStars());
            return true;
        }
        return false;
    }

    /**
     * This method deletes the hotel from the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean delete(Hotel hotel) {
        entityManager.joinTransaction();
        Optional<Hotel> hotelFound = Optional.ofNullable(entityManager.find(Hotel.class, hotel.getId()));
        if (hotelFound.isPresent()) {
            entityManager.remove(hotelFound.get());
            return true;
        }
        return false;
    }

    @Override
    public List<Hotel> query(Specification specification) {
        return null;
    }
}
