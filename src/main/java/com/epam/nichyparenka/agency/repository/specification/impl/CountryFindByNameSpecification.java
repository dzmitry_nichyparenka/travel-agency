package com.epam.nichyparenka.agency.repository.specification.impl;

import com.epam.nichyparenka.agency.entity.Country;
import com.epam.nichyparenka.agency.repository.specification.CountrySpecification;
import com.epam.nichyparenka.agency.repository.specification.SqlSpecification;

/**
 * The class implements country specification, SQL
 * specification and provides a country search by name.
 *
 * @author Dzmitry Nichyparenka
 */
public class CountryFindByNameSpecification implements CountrySpecification, SqlSpecification {
    /**
     * The field contains name of the country.
     */
    private String name;

    /**
     * This method creates country specification object with name.
     *
     * @param name name of the country
     */
    public CountryFindByNameSpecification(String name) {
        this.name = name;
    }

    @Override
    public boolean specified(Country country) {
        return name.equals(country.getName());
    }

    @Override
    public String toSqlQuery() {
        return null;
    }
}
