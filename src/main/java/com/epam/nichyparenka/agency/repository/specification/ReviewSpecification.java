package com.epam.nichyparenka.agency.repository.specification;

import com.epam.nichyparenka.agency.entity.Review;

/**
 * The interface provides specification for reviews.
 *
 * @author Dzmitry Nichyparenka
 */
public interface ReviewSpecification extends Specification {
    /**
     * This method checks existence of review (or reviews) with specified criteria.
     *
     * @param review review object
     * @return true if exists, false otherwise
     */
    boolean specified(Review review);
}
