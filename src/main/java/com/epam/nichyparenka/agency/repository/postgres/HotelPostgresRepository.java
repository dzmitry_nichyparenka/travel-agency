package com.epam.nichyparenka.agency.repository.postgres;

import com.epam.nichyparenka.agency.entity.Country;
import com.epam.nichyparenka.agency.entity.Hotel;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import com.epam.nichyparenka.agency.validation.ParametersValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The class provides implementation of Postgres repository
 * for hotels. This class implements {@link BasicRepository}.
 *
 * @author Dzmitry Nichyparenka
 */
@Repository
@Profile("!jpa")
public class HotelPostgresRepository implements BasicRepository<Hotel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(HotelPostgresRepository.class);
    private static final String SQL_SCHEMA_NAME = "\"public\"";
    private static final String SQL_CREATE_HOTEL = "INSERT INTO " + SQL_SCHEMA_NAME + ".hotels (country_id, name, phone, " +
            "stars) VALUES (?, ?, ?, ?)";
    private static final String SQL_READ_HOTEL = "SELECT country_id, name, phone, stars FROM " + SQL_SCHEMA_NAME + ".hotels " +
            "WHERE id=?";
    private static final String SQL_UPDATE_HOTEL = "UPDATE " + SQL_SCHEMA_NAME + ".hotels SET country_id=?, name=?, " +
            "phone=?, stars=? WHERE id=?";
    private static final String SQL_DELETE_HOTEL = "DELETE FROM " + SQL_SCHEMA_NAME + ".hotels WHERE id=?";
    private static final String PARAMETER_COUNTRY_ID = "country_id";
    private static final String PARAMETER_NAME = "name";
    private static final String PARAMETER_PHONE = "phone";
    private static final String PARAMETER_STARS = "stars";
    private JdbcTemplate jdbcTemplate;

    /**
     * This constructor creates a hotel postgres repository
     * object with parameters.
     *
     * @param jdbcTemplate jdbcTemplate connected to datasource
     */
    public HotelPostgresRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * This method creates the hotel in the Postgres database.
     */
    @Override
    public boolean create(Hotel hotel) {
        long id = hotel.getId();
        Country country = hotel.getCountry();
        String name = hotel.getName();
        String phoneNumber = hotel.getPhoneNumber();
        byte numberOfStars = hotel.getNumberOfStars();

        if (!ParametersValidation.validateHotel(id, country, name, phoneNumber, numberOfStars)) {
            LOGGER.info("Incorrect hotel parameters - id: {}, countryId: {}, name: {}, phone: {}, count of stars: {}.",
                    id, country.getName(), name, phoneNumber, numberOfStars);
            return false;
        }
        if (jdbcTemplate.update(SQL_CREATE_HOTEL, country.getId(), name, phoneNumber, numberOfStars) == 1) {
            LOGGER.info("New hotel has been successfully created.");
            return true;
        }
        LOGGER.info("It's impossible to create new hotel.");
        return false;
    }

    /**
     * This method reads the hotel data from the Postgres database
     * by unique identifier.
     */
    @Override
    public Optional<Hotel> read(long id) {
        if (!ParametersValidation.validateId(id)) {
            LOGGER.info("Incorrect hotel parameter - id: {}.", id);
            return Optional.empty();
        }
        try {
            return jdbcTemplate.queryForObject(SQL_READ_HOTEL, new Object[]{id},
                    (resultSet, i) -> {
                        Hotel hotel = new Hotel();
                        hotel.setId(id);
                        Country country = new Country();
                        country.setId(resultSet.getLong(PARAMETER_COUNTRY_ID));
                        hotel.setCountry(country);
                        hotel.setName(resultSet.getString(PARAMETER_NAME));
                        hotel.setPhoneNumber(resultSet.getString(PARAMETER_PHONE));
                        hotel.setNumberOfStars(resultSet.getByte(PARAMETER_STARS));
                        return Optional.of(hotel);
                    });
        } catch (EmptyResultDataAccessException exception) {
            LOGGER.info("Hotel cannot be found by this id: " + id + ".", exception);
            return Optional.empty();
        }
    }

    /**
     * This method updates the hotel data in the Postgres database.
     */
    @Override
    public boolean update(Hotel hotel) {
        long id = hotel.getId();
        Country country = hotel.getCountry();
        String name = hotel.getName();
        String phoneNumber = hotel.getPhoneNumber();
        byte numberOfStars = hotel.getNumberOfStars();

        if (!ParametersValidation.validateHotel(id, country, name, phoneNumber, numberOfStars)) {
            LOGGER.info("Incorrect hotel parameters - id: {}, country: {}, name: {}, phone: {}, count of stars: {}.",
                    id, country.getName(), name, phoneNumber, numberOfStars);
            return false;
        }
        if (jdbcTemplate.update(SQL_UPDATE_HOTEL, hotel.getId(), name, phoneNumber, numberOfStars, id) == 1) {
            LOGGER.info("Hotel (id {}) has been successfully updated.", id);
            return true;
        } else {
            LOGGER.info("It's impossible to update hotel by this id: {}.", id);
            return false;
        }
    }

    /**
     * This method deletes the hotel from the Postgres database.
     */
    @Override
    public boolean delete(Hotel hotel) {
        long id = hotel.getId();

        if (!ParametersValidation.validateId(id)) {
            LOGGER.info("Incorrect hotel id: {}.", id);
            return false;
        }
        if (jdbcTemplate.update(SQL_DELETE_HOTEL, id) == 1) {
            LOGGER.info("Hotel (id {}) has been successfully deleted.", id);
            return true;
        } else {
            LOGGER.info("It's impossible to delete hotel by this id: {}.", id);
            return false;
        }
    }

    /**
     * This method executes the specified query on Postgres database.
     */
    @Override
    public List<Hotel> query(Specification specification) {
        return null;
    }
}
