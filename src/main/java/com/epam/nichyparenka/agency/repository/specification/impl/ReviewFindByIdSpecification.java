package com.epam.nichyparenka.agency.repository.specification.impl;

import com.epam.nichyparenka.agency.entity.Review;
import com.epam.nichyparenka.agency.repository.specification.ReviewSpecification;
import com.epam.nichyparenka.agency.repository.specification.SqlSpecification;

/**
 * The class implements review specification, SQL
 * specification and provides a review search by id.
 *
 * @author Dzmitry Nichyparenka
 */
public class ReviewFindByIdSpecification implements ReviewSpecification, SqlSpecification {
    /**
     * The field contains unique identifier of the review.
     */
    private int id;

    /**
     * This method creates review specification object with id.
     *
     * @param id unique identifier of the review
     */
    public ReviewFindByIdSpecification(int id) {
        this.id = id;
    }

    @Override
    public boolean specified(Review review) {
        return id == review.getId();
    }

    @Override
    public String toSqlQuery() {
        return null;
    }
}
