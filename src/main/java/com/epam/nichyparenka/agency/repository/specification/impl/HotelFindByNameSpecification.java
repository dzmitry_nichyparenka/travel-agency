package com.epam.nichyparenka.agency.repository.specification.impl;

import com.epam.nichyparenka.agency.entity.Hotel;
import com.epam.nichyparenka.agency.repository.specification.HotelSpecification;
import com.epam.nichyparenka.agency.repository.specification.SqlSpecification;

/**
 * The class implements hotel specification, SQL
 * specification and provides a hotel search by name.
 *
 * @author Dzmitry Nichyparenka
 */
public class HotelFindByNameSpecification implements HotelSpecification, SqlSpecification {
    /**
     * The field contains name of the hotel.
     */
    private String name;

    /**
     * This method creates hotel specification object with name.
     *
     * @param name name of the hotel
     */
    public HotelFindByNameSpecification(String name) {
        this.name = name;
    }

    @Override
    public boolean specified(Hotel hotel) {
        return name.equals(hotel.getName());
    }

    @Override
    public String toSqlQuery() {
        return null;
    }
}
