package com.epam.nichyparenka.agency.repository.specification;

import com.epam.nichyparenka.agency.entity.Country;

/**
 * The interface provides specification for countries.
 *
 * @author Dzmitry Nichyparenka
 */
public interface CountrySpecification extends Specification {
    /**
     * This method checks existence of country (or countries) with specified criteria.
     *
     * @param country country object
     * @return true if exists, false otherwise
     */
    boolean specified(Country country);
}
