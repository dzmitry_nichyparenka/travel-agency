package com.epam.nichyparenka.agency.repository.jpa;

import com.epam.nichyparenka.agency.entity.Country;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

/**
 * The class provides implementation of JPA repository for
 * countries. This class implements {@link BasicRepository}.
 *
 * @author Dzmitry Nichyparenka
 */
@Repository
@Profile({"!postgres"})
public class CountryJpaRepository implements BasicRepository<Country> {
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * This method creates the country in the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean create(Country country) {
        entityManager.joinTransaction();
        entityManager.persist(country);
        return true;
    }

    /**
     * This method reads the country data from the database
     * associated with current JPA implementation.
     */
    @Override
    public Optional<Country> read(long id) {
        return Optional.ofNullable(entityManager.find(Country.class, id));
    }

    /**
     * This method updates the country in the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean update(Country country) {
        entityManager.joinTransaction();
        Optional<Country> countryOptional = Optional.ofNullable(entityManager.find(Country.class, country.getId()));
        if (countryOptional.isPresent()) {
            countryOptional.get().setName(country.getName());
            return true;
        }
        return false;
    }

    /**
     * This method deletes the country from the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean delete(Country country) {
        entityManager.joinTransaction();
        Optional<Country> countryOptional = Optional.ofNullable(entityManager.find(Country.class, country.getId()));
        if (countryOptional.isPresent()) {
            entityManager.remove(countryOptional.get());
            return true;
        }
        return false;
    }

    @Override
    public List<Country> query(Specification specification) {
        return null;
    }
}
