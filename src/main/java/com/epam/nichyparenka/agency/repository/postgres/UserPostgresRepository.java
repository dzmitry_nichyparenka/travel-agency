package com.epam.nichyparenka.agency.repository.postgres;

import com.epam.nichyparenka.agency.entity.User;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import com.epam.nichyparenka.agency.validation.ParametersValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The class provides implementation of Postgres repository
 * for users. This class implements {@link BasicRepository}.
 *
 * @author Dzmitry Nichyparenka
 */
@Repository
@Profile("!jpa")
public class UserPostgresRepository implements BasicRepository<User> {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserPostgresRepository.class);
    private static final String SQL_SCHEMA_NAME = "\"public\"";
    private static final String SQL_CREATE_USER = "INSERT INTO " + SQL_SCHEMA_NAME + ".users (login, password, email, " +
            "first_name, last_name) VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_READ_USER = "SELECT login, password, email, first_name, last_name FROM " +
            SQL_SCHEMA_NAME + ".users WHERE id=?";
    private static final String SQL_UPDATE_USER = "UPDATE " + SQL_SCHEMA_NAME + ".users SET login=?, password=?, email=?, " +
            "first_name=?, last_name=? WHERE id=?";
    private static final String SQL_DELETE_USER = "DELETE FROM " + SQL_SCHEMA_NAME + ".users WHERE id=?";
    private static final String PARAMETER_LOGIN = "login";
    private static final String PARAMETER_PASSWORD = "password";
    private static final String PARAMETER_EMAIL = "email";
    private static final String PARAMETER_FIRST_NAME = "first_name";
    private static final String PARAMETER_LAST_NAME = "last_name";
    private JdbcTemplate jdbcTemplate;

    /**
     * This constructor creates a user postgres repository
     * object with parameters.
     *
     * @param jdbcTemplate jdbcTemplate connected to datasource
     */
    public UserPostgresRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * This method creates the user in the Postgres database.
     */
    @Override
    public boolean create(User user) {
        long id = user.getId();
        String login = user.getLogin();
        String password = user.getPassword();
        String email = user.getEmail();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();

        if (!ParametersValidation.validateUser(id, login, password, email, firstName, lastName)) {
            LOGGER.info("Incorrect user parameters - id: {}, login: {}, password: {}, email: {}, " +
                    "firstName: {}, lastName: {}.", id, login, password, email, firstName, lastName);
            return false;
        }
        if (jdbcTemplate.update(SQL_CREATE_USER, login, password, email, firstName, lastName) == 1) {
            LOGGER.info("New user has been successfully created.");
            return true;
        }
        LOGGER.info("It's impossible to create new user.");
        return false;
    }

    /**
     * This method reads the user data from the Postgres database
     * by unique identifier.
     */
    @Override
    public Optional<User> read(long id) {
        if (!ParametersValidation.validateId(id)) {
            LOGGER.info("Incorrect user parameter - id: {}.", id);
            return Optional.empty();
        }
        try {
            return jdbcTemplate.queryForObject(SQL_READ_USER, new Object[]{id},
                    (resultSet, i) -> {
                        User currentUser = new User();
                        currentUser.setId(id);
                        currentUser.setLogin(resultSet.getString(PARAMETER_LOGIN));
                        currentUser.setPassword(resultSet.getString(PARAMETER_PASSWORD));
                        currentUser.setEmail(resultSet.getString(PARAMETER_EMAIL));
                        currentUser.setFirstName(resultSet.getString(PARAMETER_FIRST_NAME));
                        currentUser.setLastName(resultSet.getString(PARAMETER_LAST_NAME));
                        return Optional.of(currentUser);
                    });
        } catch (EmptyResultDataAccessException exception) {
            LOGGER.info("User cannot be found by this id: " + id + ".", exception);
            return Optional.empty();
        }
    }

    /**
     * This method updates the user data in the Postgres database.
     */
    @Override
    public boolean update(User user) {
        long id = user.getId();
        String login = user.getLogin();
        String password = user.getPassword();
        String email = user.getEmail();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();

        if (!ParametersValidation.validateUser(id, login, password, email, firstName, lastName)) {
            LOGGER.info("Incorrect user parameters - id: {}, login: {}, password: {}, email: {}, " +
                    "firstName: {}, lastName: {}.", id, login, password, email, firstName, lastName);
            return false;
        }
        if (jdbcTemplate.update(SQL_UPDATE_USER, login, password, email, firstName, lastName, id) == 1) {
            LOGGER.info("User (id {}) has been successfully updated.", id);
            return true;
        } else {
            LOGGER.info("It's impossible to update user by this id: {}.", id);
            return false;
        }
    }

    /**
     * This method deletes the user from the Postgres database.
     */
    @Override
    public boolean delete(User user) {
        long id = user.getId();

        if (!ParametersValidation.validateId(id)) {
            LOGGER.info("Incorrect user id: {}.", id);
            return false;
        }
        if (jdbcTemplate.update(SQL_DELETE_USER, id) == 1) {
            LOGGER.info("User (id {}) has been successfully deleted.", id);
            return true;
        } else {
            LOGGER.info("It's impossible to delete user by this id: {}.", id);
            return false;
        }
    }

    /**
     * This method executes the specified query on Postgres database.
     */
    @Override
    public List<User> query(Specification specification) {
        return null;
    }
}
