package com.epam.nichyparenka.agency.repository.jpa;

import com.epam.nichyparenka.agency.entity.Tour;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

/**
 * The class provides implementation of JPA repository for
 * tours. This class implements {@link BasicRepository}.
 *
 * @author Dzmitry Nichyparenka
 */
@Repository
@Profile({"!postgres"})
public class TourJpaRepository implements BasicRepository<Tour> {
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * This method creates the tour in the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean create(Tour tour) {
        entityManager.joinTransaction();
        entityManager.persist(tour);
        return true;
    }

    /**
     * This method reads the tour data from the database
     * associated with current JPA implementation.
     */
    @Override
    public Optional<Tour> read(long id) {
        return Optional.ofNullable(entityManager.find(Tour.class, id));
    }

    /**
     * This method updates the tour in the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean update(Tour tour) {
        entityManager.joinTransaction();
        Optional<Tour> tourOptional = Optional.ofNullable(entityManager.find(Tour.class, tour.getId()));
        if (tourOptional.isPresent()) {
            Tour tourFound = tourOptional.get();
            tourFound.setName(tour.getName());
            tourFound.setDescription(tour.getDescription());
            tourFound.setDate(tour.getDate());
            tourFound.setDuration(tour.getDuration());
            tourFound.setType(tour.getType());
            tourFound.setCountry(tour.getCountry());
            tourFound.setHotel(tour.getHotel());
            tourFound.setPhoto(tour.getPhoto());
            tourFound.setCost(tour.getCost());
            return true;
        }
        return false;
    }

    /**
     * This method deletes the tour from the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean delete(Tour tour) {
        entityManager.joinTransaction();
        Optional<Tour> tourOptional = Optional.ofNullable(entityManager.find(Tour.class, tour.getId()));
        if (tourOptional.isPresent()) {
            entityManager.remove(tourOptional.get());
            return true;
        }
        return false;
    }

    @Override
    public List<Tour> query(Specification specification) {
        return null;
    }
}
