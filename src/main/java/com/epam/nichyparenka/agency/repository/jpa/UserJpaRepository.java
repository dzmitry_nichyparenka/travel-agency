package com.epam.nichyparenka.agency.repository.jpa;

import com.epam.nichyparenka.agency.entity.User;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

/**
 * The class provides implementation of JPA repository for
 * users. This class implements {@link BasicRepository}.
 *
 * @author Dzmitry Nichyparenka
 */
@Repository
@Profile({"!postgres"})
public class UserJpaRepository implements BasicRepository<User> {
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * This method creates the user in the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean create(User user) {
        entityManager.joinTransaction();
        entityManager.persist(user);
        return true;
    }

    /**
     * This method reads the user data from the database
     * associated with current JPA implementation.
     */
    @Override
    public Optional<User> read(long id) {
        return Optional.ofNullable(entityManager.find(User.class, id));
    }

    /**
     * This method updates the user in the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean update(User user) {
        entityManager.joinTransaction();
        Optional<User> userOptional = Optional.ofNullable(entityManager.find(User.class, user.getId()));
        if (userOptional.isPresent()) {
            User userFound = userOptional.get();
            userFound.setLogin(user.getLogin());
            userFound.setPassword(user.getPassword());
            userFound.setFirstName(user.getFirstName());
            userFound.setLastName(user.getLastName());
            return true;
        }
        return false;
    }

    /**
     * This method deletes the user from the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean delete(User user) {
        entityManager.joinTransaction();
        Optional<User> optionalUser = Optional.ofNullable(entityManager.find(User.class, user.getId()));
        if (optionalUser.isPresent()) {
            entityManager.remove(optionalUser.get());
            return true;
        }
        return false;
    }

    @Override
    public List<User> query(Specification specification) {
        return null;
    }
}
