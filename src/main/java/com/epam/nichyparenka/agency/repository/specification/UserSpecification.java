package com.epam.nichyparenka.agency.repository.specification;

import com.epam.nichyparenka.agency.entity.User;

/**
 * The interface provides specification for countries.
 *
 * @author Dzmitry Nichyparenka
 */
public interface UserSpecification extends Specification {
    /**
     * This method checks existence of user (or users) with specified criteria.
     *
     * @param user user object
     * @return true if exists, false otherwise
     */
    boolean specified(User user);
}
