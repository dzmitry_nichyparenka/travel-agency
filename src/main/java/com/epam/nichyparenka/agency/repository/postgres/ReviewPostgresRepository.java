package com.epam.nichyparenka.agency.repository.postgres;

import com.epam.nichyparenka.agency.entity.Review;
import com.epam.nichyparenka.agency.entity.Tour;
import com.epam.nichyparenka.agency.entity.User;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import com.epam.nichyparenka.agency.validation.ParametersValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The class provides implementation of Postgres repository
 * for reviews. This class implements {@link BasicRepository}.
 *
 * @author Dzmitry Nichyparenka
 */
@Repository
@Profile("!jpa")
public class ReviewPostgresRepository implements BasicRepository<Review> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReviewPostgresRepository.class);
    private static final String SQL_SCHEMA_NAME = "\"public\"";
    private static final String SQL_CREATE_REVIEW = "INSERT INTO " + SQL_SCHEMA_NAME + ".reviews (tour_id, user_id, text) " +
            "VALUES (?, ?, ?)";
    private static final String SQL_READ_REVIEW = "SELECT tour_id, user_id, text FROM " + SQL_SCHEMA_NAME + ".reviews " +
            "WHERE id=?";
    private static final String SQL_UPDATE_REVIEW = "UPDATE " + SQL_SCHEMA_NAME + ".reviews SET tour_id=?, user_id=?, " +
            "text=? WHERE id=?";
    private static final String SQL_DELETE_REVIEW = "DELETE FROM " + SQL_SCHEMA_NAME + ".reviews WHERE id=?";
    private static final String PARAMETER_TEXT = "text";
    private static final String PARAMETER_TOUR_ID = "tour_id";
    private static final String PARAMETER_USER_ID = "user_id";
    private JdbcTemplate jdbcTemplate;

    /**
     * This constructor creates a review postgres repository
     * object with parameters.
     *
     * @param jdbcTemplate jdbcTemplate connected to datasource
     */
    public ReviewPostgresRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * This method creates the review in the Postgres database.
     */
    @Override
    public boolean create(Review review) {
        Tour tour = review.getTour();
        User user = review.getUser();
        String text = review.getText();

        if (!ParametersValidation.validateReview(tour, user, text)) {
            LOGGER.info("Incorrect review parameters - tour: {}, user: {}, text: {}.", tour, user, text);
            return false;
        }
        if (jdbcTemplate.update(SQL_CREATE_REVIEW, tour.getId(), user.getId(), text) == 1) {
            LOGGER.info("New review has been successfully created.");
            return true;
        }
        LOGGER.info("It's impossible to create new review.");
        return false;
    }

    /**
     * This method reads the review data from the Postgres database
     * by unique identifier.
     */
    @Override
    public Optional<Review> read(long id) {
        if (!ParametersValidation.validateId(id)) {
            LOGGER.info("Incorrect review parameter - id: {}.", id);
            return Optional.empty();
        }
        try {
            return jdbcTemplate.queryForObject(SQL_READ_REVIEW, new Object[]{id},
                    (resultSet, i) -> {
                        Review review = new Review();
                        review.setId(id);
                        review.setText(resultSet.getString(PARAMETER_TEXT));

                        Tour tour = new Tour();
                        tour.setId(resultSet.getLong(PARAMETER_TOUR_ID));
                        User user = new User();
                        user.setId(resultSet.getLong(PARAMETER_USER_ID));

                        review.setTour(tour);
                        review.setUser(user);
                        return Optional.of(review);
                    });
        } catch (EmptyResultDataAccessException exception) {
            LOGGER.info("Review cannot be found by this id: " + id + ".", exception);
            return Optional.empty();
        }
    }

    /**
     * This method updates the review data in the Postgres database.
     */
    @Override
    public boolean update(Review review) {
        long id = review.getId();
        Tour tour = review.getTour();
        User user = review.getUser();
        String text = review.getText();

        if (!ParametersValidation.validateReview(tour, user, text)) {
            LOGGER.info("Incorrect review parameters - tourId: {}, userId: {}, text: {}.", tour, user, text);
            return false;
        }
        if (jdbcTemplate.update(SQL_UPDATE_REVIEW, tour.getId(), user.getId(), text, id) == 1) {
            LOGGER.info("Review (id {}) has been successfully updated.", id);
            return true;
        } else {
            LOGGER.info("It's impossible to update review by this id: {}.", id);
            return false;
        }
    }

    /**
     * This method deletes the review from the Postgres database.
     */
    @Override
    public boolean delete(Review review) {
        long id = review.getId();

        if (!ParametersValidation.validateId(id)) {
            LOGGER.info("Incorrect review id: {}.", id);
            return false;
        }
        if (jdbcTemplate.update(SQL_DELETE_REVIEW, id) == 1) {
            LOGGER.info("Review (id {}) has been successfully deleted.", id);
            return true;
        } else {
            LOGGER.info("It's impossible to delete review by this id: {}.", id);
            return false;
        }
    }

    /**
     * This method executes the specified query on Postgres database.
     */
    @Override
    public List<Review> query(Specification specification) {
        return null;
    }
}
