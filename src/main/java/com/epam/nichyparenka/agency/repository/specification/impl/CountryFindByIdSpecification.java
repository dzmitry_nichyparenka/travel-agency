package com.epam.nichyparenka.agency.repository.specification.impl;

import com.epam.nichyparenka.agency.entity.Country;
import com.epam.nichyparenka.agency.repository.specification.CountrySpecification;
import com.epam.nichyparenka.agency.repository.specification.SqlSpecification;

/**
 * The class implements country specification, SQL
 * specification and provides a country search by id.
 *
 * @author Dzmitry Nichyparenka
 */
public class CountryFindByIdSpecification implements CountrySpecification, SqlSpecification {
    /**
     * The field contains unique identifier of the country.
     */
    private int id;

    /**
     * This method creates country specification object with id.
     *
     * @param id unique identifier of the country
     */
    public CountryFindByIdSpecification(int id) {
        this.id = id;
    }

    @Override
    public boolean specified(Country country) {
        return id == country.getId();
    }

    @Override
    public String toSqlQuery() {
        return null;
    }
}
