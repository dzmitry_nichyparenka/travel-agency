package com.epam.nichyparenka.agency.repository.jpa;

import com.epam.nichyparenka.agency.entity.Review;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

/**
 * The class provides implementation of JPA repository for
 * reviews. This class implements {@link BasicRepository}.
 *
 * @author Dzmitry Nichyparenka
 */
@Repository
@Profile({"!postgres"})
public class ReviewJpaRepository implements BasicRepository<Review> {
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * This method creates the review in the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean create(Review review) {
        entityManager.joinTransaction();
        entityManager.persist(review);
        return true;
    }

    /**
     * This method reads the review data from the database
     * associated with current JPA implementation.
     */
    @Override
    public Optional<Review> read(long id) {
        return Optional.ofNullable(entityManager.find(Review.class, id));
    }

    /**
     * This method updates the review in the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean update(Review review) {
        entityManager.joinTransaction();
        Optional<Review> reviewOptional = Optional.ofNullable(entityManager.find(Review.class, review.getId()));
        if (reviewOptional.isPresent()) {
            Review reviewFound = reviewOptional.get();
            reviewFound.setUser(review.getUser());
            reviewFound.setTour(reviewFound.getTour());
            reviewFound.setText(review.getText());
            return true;
        }
        return false;
    }

    /**
     * This method deletes the review from the database associated
     * with current JPA implementation.
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean delete(Review review) {
        entityManager.joinTransaction();
        Optional<Review> reviewFound = Optional.ofNullable(entityManager.find(Review.class, review.getId()));
        if (reviewFound.isPresent()) {
            entityManager.remove(reviewFound.get());
            return true;
        }
        return false;
    }

    @Override
    public List<Review> query(Specification specification) {
        return null;
    }
}
