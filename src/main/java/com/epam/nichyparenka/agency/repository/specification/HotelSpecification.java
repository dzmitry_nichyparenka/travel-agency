package com.epam.nichyparenka.agency.repository.specification;

import com.epam.nichyparenka.agency.entity.Hotel;

/**
 * The interface provides specification for hotels.
 *
 * @author Dzmitry Nichyparenka
 */
public interface HotelSpecification extends Specification {
    /**
     * This method checks existence of hotel (or hotels) with specified criteria.
     *
     * @param hotel hotel object
     * @return true if exists, false otherwise
     */
    boolean specified(Hotel hotel);
}
