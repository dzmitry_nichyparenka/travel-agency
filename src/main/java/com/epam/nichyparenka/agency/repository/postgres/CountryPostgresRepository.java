package com.epam.nichyparenka.agency.repository.postgres;

import com.epam.nichyparenka.agency.entity.Country;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import com.epam.nichyparenka.agency.validation.ParametersValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * The class provides implementation of Postgres repository
 * for countries. This class implements {@link BasicRepository}.
 *
 * @author Dzmitry Nichyparenka
 */
@Repository
@Profile("!jpa")
public class CountryPostgresRepository implements BasicRepository<Country> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CountryPostgresRepository.class);
    private static final String SQL_SCHEMA_NAME = "\"public\"";
    private static final String SQL_CREATE_COUNTRY = "INSERT INTO " + SQL_SCHEMA_NAME + ".countries (name) VALUES (?)";
    private static final String SQL_READ_COUNTRY = "SELECT name FROM " + SQL_SCHEMA_NAME + ". countries WHERE id=?";
    private static final String SQL_UPDATE_COUNTRY = "UPDATE " + SQL_SCHEMA_NAME + ".countries SET name=? WHERE id=?";
    private static final String SQL_DELETE_COUNTRY = "DELETE FROM " + SQL_SCHEMA_NAME + ".countries WHERE id=?";
    private static final String PARAMETER_NAME = "name";
    private JdbcTemplate jdbcTemplate;

    /**
     * This constructor creates a country postgres repository
     * object with parameters.
     *
     * @param jdbcTemplate jdbcTemplate connected to datasource
     */
    public CountryPostgresRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * This method creates the country in the Postgres database.
     */
    @Override
    public boolean create(Country country) {
        long id = country.getId();
        String name = country.getName();

        if (!ParametersValidation.validateCountry(id, name)) {
            LOGGER.info("Incorrect country parameters - id: {}, name: {}.", id, name);
            return false;
        }
        if (jdbcTemplate.update(SQL_CREATE_COUNTRY, country.getName()) == 1) {
            LOGGER.info("New country has been successfully created.");
            return true;
        }
        LOGGER.info("It's impossible to create new country.");
        return false;
    }

    /**
     * This method reads the country data from the Postgres database
     * by unique identifier.
     */
    @Override
    public Optional<Country> read(long id) {
        if (!ParametersValidation.validateId(id)) {
            LOGGER.info("Incorrect country parameter - id: {}.", id);
            return Optional.empty();
        }
        try {
            return jdbcTemplate.queryForObject(SQL_READ_COUNTRY, new Object[]{id},
                    (resultSet, i) -> {
                        Country country = new Country();
                        country.setId(id);
                        country.setName(resultSet.getString(PARAMETER_NAME));
                        return Optional.of(country);
                    });
        } catch (EmptyResultDataAccessException exception) {
            LOGGER.info("Country cannot be found by this id: " + id + ".", exception);
            return Optional.empty();
        }
    }

    /**
     * This method updates the country data in the Postgres database.
     */
    @Override
    public boolean update(Country country) {
        long id = country.getId();
        String name = country.getName();

        if (!ParametersValidation.validateCountry(id, name)) {
            LOGGER.info("Incorrect country parameters - id: {}, name: {}.", id, name);
            return false;
        }
        if (jdbcTemplate.update(SQL_UPDATE_COUNTRY, name, id) == 1) {
            LOGGER.info("Country (id {}) has been successfully updated.", id);
            return true;
        } else {
            LOGGER.info("It's impossible to update country by this id: {}.", id);
            return false;
        }
    }

    /**
     * This method deletes the country from the Postgres database.
     */
    @Override
    public boolean delete(Country country) {
        long id = country.getId();

        if (!ParametersValidation.validateId(id)) {
            LOGGER.info("Incorrect country id: {}.", id);
            return false;
        }
        if (jdbcTemplate.update(SQL_DELETE_COUNTRY, id) == 1) {
            LOGGER.info("Country (id {}) has been successfully deleted.", id);
            return true;
        } else {
            LOGGER.info("It's impossible to delete country by this id: {}.", id);
            return false;
        }
    }

    /**
     * This method executes the specified query on Postgres database.
     */
    @Override
    public List<Country> query(Specification specification) {
        return null;
    }
}
