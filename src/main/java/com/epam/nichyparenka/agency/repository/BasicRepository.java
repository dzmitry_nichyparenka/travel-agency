package com.epam.nichyparenka.agency.repository;

import java.util.List;
import java.util.Optional;

import com.epam.nichyparenka.agency.entity.BasicEntity;
import com.epam.nichyparenka.agency.repository.specification.Specification;

/**
 * The interface provides basic CRUD operations for all
 * repositories that implement this interface.
 *
 * @param <E> concrete entity implementation of {@link BasicEntity} class
 * @author Dzmitry Nichyparenka
 */
public interface BasicRepository<E extends BasicEntity> {
    /**
     * This method creates a data record from this entity
     * in the repository.
     *
     * @param entity entity with data for creating record
     * @return true if operation was successful, false otherwise
     */
    boolean create(E entity);

    /**
     * This method reads an entity from the data storage.
     *
     * @param id unique identifier of the entity
     * @return found entity
     */
    Optional<E> read(long id);

    /**
     * This method updates data of the entity in the data storage.
     *
     * @param entity entity to update
     * @return true if operation was successful, false otherwise
     */
    boolean update(E entity);

    /**
     * This method deletes an entity if it exists in the data storage.
     *
     * @param entity to delete
     * @return true if operation was successful, false otherwise
     */
    boolean delete(E entity);

    /**
     * This method executes query which performs specific action
     * on entity or on entities in the data storage.
     *
     * @param specification concrete specific action
     * @return list of entities, depends on specification parameter
     */
    List<E> query(Specification specification);
}
