package com.epam.nichyparenka.agency.repository.specification.impl;

import com.epam.nichyparenka.agency.entity.User;
import com.epam.nichyparenka.agency.repository.specification.SqlSpecification;
import com.epam.nichyparenka.agency.repository.specification.UserSpecification;

/**
 * The class implements user specification, SQL
 * specification and provides a user search by id.
 *
 * @author Dzmitry Nichyparenka
 */
public class UserFindByIdSpecification implements UserSpecification, SqlSpecification {
    /**
     * The field contains the unique identifier of the user.
     */
    private int id;

    /**
     * This method creates user specification object with id.
     *
     * @param id unique identifier of the user
     */
    public UserFindByIdSpecification(int id) {
        this.id = id;
    }

    @Override
    public boolean specified(User user) {
        return id == user.getId();
    }

    @Override
    public String toSqlQuery() {
        return null;
    }
}
