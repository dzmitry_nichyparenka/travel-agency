package com.epam.nichyparenka.agency.repository.specification;

/**
 * The interface provides abstraction for the SQL specification.
 *
 * @author Dzmitry Nichyparenka
 */
public interface SqlSpecification extends Specification {
    /**
     * Retrieves an SQL query.
     *
     * @return result SQL query as a string
     */
    String toSqlQuery();
}
