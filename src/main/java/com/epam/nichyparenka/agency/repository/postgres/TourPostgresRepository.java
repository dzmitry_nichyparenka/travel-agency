package com.epam.nichyparenka.agency.repository.postgres;

import com.epam.nichyparenka.agency.entity.Country;
import com.epam.nichyparenka.agency.entity.Hotel;
import com.epam.nichyparenka.agency.entity.Tour;
import com.epam.nichyparenka.agency.entity.TourType;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import com.epam.nichyparenka.agency.validation.ParametersValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Optional;

/**
 * The class provides implementation of Postgres repository
 * for tours. This class implements {@link BasicRepository}.
 *
 * @author Dzmitry Nichyparenka
 */
@Repository
@Profile("!jpa")
public class TourPostgresRepository implements BasicRepository<Tour> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TourPostgresRepository.class);
    private static final String SQL_SCHEMA_NAME = "\"public\"";
    private static final String SQL_CREATE_TOUR = "INSERT INTO " + SQL_SCHEMA_NAME + ".tours (country_id, hotel_id, type, " +
            "name, description, photo, start_date, duration, cost) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_READ_TOUR = "SELECT id, country_id, hotel_id, type, name, description, photo, " +
            "start_date, duration, cost FROM " + SQL_SCHEMA_NAME + ".tours WHERE id=?";
    private static final String SQL_UPDATE_TOUR = "UPDATE " + SQL_SCHEMA_NAME + ".tours SET country_id=?, hotel_id=?, " +
            "type=?, name=?, description=?, photo=?, start_date=?, duration=?, cost=? WHERE id=?";
    private static final String SQL_DELETE_TOUR = "DELETE FROM " + SQL_SCHEMA_NAME + ".tours WHERE id=?";
    private static final String PARAMETER_TYPE = "type";
    private static final String PARAMETER_NAME = "name";
    private static final String PARAMETER_DESCRIPTION = "description";
    private static final String PARAMETER_PHOTO = "photo";
    private static final String PARAMETER_DATE = "start_date";
    private static final String PARAMETER_DURATION = "duration";
    private static final String PARAMETER_COST = "cost";
    private static final String PARAMETER_COUNTRY_ID = "country_id";
    private static final String PARAMETER_HOTEL_ID = "hotel_id";
    private static final int COST_SCALE = 2;
    private JdbcTemplate jdbcTemplate;

    /**
     * This constructor creates a tour postgres repository
     * object with parameters.
     *
     * @param jdbcTemplate jdbcTemplate connected to datasource
     */
    public TourPostgresRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * This method creates the tour in the Postgres database.
     */
    @Override
    public boolean create(Tour tour) {
        long id = tour.getId();
        Country country = tour.getCountry();
        Hotel hotel = tour.getHotel();
        TourType type = tour.getType();
        String name = tour.getName();
        String description = tour.getDescription();
        byte[] photo = tour.getPhoto();
        LocalDate date = tour.getDate();
        Period duration = tour.getDuration();
        BigDecimal cost = tour.getCost();

        if (!ParametersValidation.validateTour(id, country, hotel, type, name, description, date, duration, cost)) {
            LOGGER.info("Incorrect tour parameters - id: {}, countryId: {}, hotelId: {}, type: {}, name: {}, description: {}, " +
                            "date: {}, duration: {}, cost: {}.", id, country.getId(), hotel.getId(), type, name, description, date,
                    duration != null ? duration.getDays() : null, cost.setScale(COST_SCALE, BigDecimal.ROUND_HALF_UP));
            return false;
        }
        if (jdbcTemplate.update(SQL_CREATE_TOUR, country.getId(), hotel.getId(), type.toString(), name, description, photo,
                date, duration.getDays(), cost.setScale(COST_SCALE, BigDecimal.ROUND_HALF_UP)) == 1) {
            LOGGER.info("New tour has been successfully created.");
            return true;
        }
        LOGGER.info("It's impossible to create new tour.");
        return false;
    }

    /**
     * This method reads the tour data from the Postgres database
     * by unique identifier.
     */
    @Override
    public Optional<Tour> read(long id) {
        if (!ParametersValidation.validateId(id)) {
            LOGGER.info("Incorrect tour parameter - id: {}.", id);
            return Optional.empty();
        }
        try {
            return jdbcTemplate.queryForObject(SQL_READ_TOUR, new Object[]{id},
                    (resultSet, i) -> {
                        Tour tour = new Tour();
                        tour.setId(id);
                        tour.setType((TourType.valueOf(resultSet.getString(PARAMETER_TYPE))));
                        tour.setName(resultSet.getString(PARAMETER_NAME));
                        tour.setDescription(resultSet.getString(PARAMETER_DESCRIPTION));
                        tour.setPhoto(resultSet.getBytes(PARAMETER_PHOTO));
                        tour.setDate(resultSet.getTimestamp(PARAMETER_DATE).toLocalDateTime().toLocalDate());
                        tour.setDuration(Period.ofDays(resultSet.getInt(PARAMETER_DURATION)));
                        tour.setCost(resultSet.getBigDecimal(PARAMETER_COST).setScale(COST_SCALE, BigDecimal.ROUND_HALF_UP));

                        Country country = new Country();
                        country.setId(resultSet.getLong(PARAMETER_COUNTRY_ID));

                        Hotel hotel = new Hotel();
                        hotel.setId(resultSet.getLong(PARAMETER_HOTEL_ID));

                        tour.setCountry(country);
                        tour.setHotel(hotel);
                        return Optional.of(tour);
                    });
        } catch (EmptyResultDataAccessException exception) {
            LOGGER.info("Tour cannot be found by this id: " + id + ".", exception);
            return Optional.empty();
        }
    }

    /**
     * This method updates the tour data in the Postgres database.
     */
    @Override
    public boolean update(Tour tour) {
        long id = tour.getId();
        Country country = tour.getCountry();
        Hotel hotel = tour.getHotel();
        TourType type = tour.getType();
        String name = tour.getName();
        String description = tour.getDescription();
        byte[] photo = tour.getPhoto();
        LocalDate date = tour.getDate();
        Period duration = tour.getDuration();
        BigDecimal cost = tour.getCost();

        if (!ParametersValidation.validateTour(id, country, hotel, type, name, description, date, duration, cost)) {
            LOGGER.info("Incorrect tour parameters - id: {}, countryId: {}, hotelId: {}, type: {}, name: {}, description: {}, " +
                            "date: {}, duration: {}, cost: {}.", id, country.getId(), hotel.getId(), type, name, description, date,
                    duration != null ? duration.getDays() : null, cost.setScale(COST_SCALE, BigDecimal.ROUND_HALF_UP));
            return false;
        }
        if (jdbcTemplate.update(SQL_UPDATE_TOUR, country.getId(), hotel.getId(), type.toString(), name, description, photo,
                date, duration.getDays(), cost.setScale(COST_SCALE, BigDecimal.ROUND_HALF_UP), id) == 1) {
            LOGGER.info("Tour (id {}) has been successfully updated.", id);
            return true;
        } else {
            LOGGER.info("It's impossible to update tour by this id: {}.", id);
            return false;
        }
    }

    /**
     * This method deletes the tour from the Postgres database.
     */
    @Override
    public boolean delete(Tour tour) {
        long id = tour.getId();

        if (!ParametersValidation.validateId(id)) {
            LOGGER.info("Incorrect hotel id: {}.", id);
            return false;
        }
        if (jdbcTemplate.update(SQL_DELETE_TOUR, id) == 1) {
            LOGGER.info("Tour (id {}) has been successfully deleted.", id);
            return true;
        } else {
            LOGGER.info("It's impossible to delete tour by this id: {}.", id);
            return false;
        }
    }

    /**
     * This method executes the specified query on Postgres database.
     */
    @Override
    public List<Tour> query(Specification specification) {
        return null;
    }
}
