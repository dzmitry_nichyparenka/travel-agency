package com.epam.nichyparenka.agency.repository.specification;

import com.epam.nichyparenka.agency.entity.Tour;

/**
 * The interface provides specification for tours.
 *
 * @author Dzmitry Nichyparenka
 */
public interface TourSpecification extends Specification {
    /**
     * This method checks existence of tour (or tours) with specified criteria.
     *
     * @param tour tour object
     * @return true if exists, false otherwise
     */
    boolean specified(Tour tour);
}
