package com.epam.nichyparenka.agency.validation;

import com.epam.nichyparenka.agency.entity.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

/**
 * The class provides validation methods for parameters
 * before all queries to storage.
 *
 * @author Dzmitry Nichyparenka
 */
public final class ParametersValidation {
    /**
     * This method validates parameters needed for updating country in the storage.
     *
     * @param id   unique country identifier
     * @param name name of the country
     * @return true if validation was successful, false otherwise
     */
    public static boolean validateCountry(long id, String name) {
        return id > 0 && name != null;
    }

    /**
     * This method validates parameters needed for updating hotel in the storage.
     *
     * @param id            unique hotel identifier
     * @param name          name of the hotel
     * @param phoneNumber   hotel phone number
     * @param numberOfStars hotel rating
     * @return true if validation was successful, false otherwise
     */
    public static boolean validateHotel(long id, Country country, String name, String phoneNumber, int numberOfStars) {
        return id > 0 && country != null && name != null && phoneNumber != null && numberOfStars >= 1 && numberOfStars <= 5;
    }

    /**
     * This method validates parameters needed for updating review in the storage.
     *
     * @param tour tour associated with this review
     * @param user user associated with this review
     * @param text text of the review
     * @return true if validation was successful, false otherwise
     */
    public static boolean validateReview(Tour tour, User user, String text) {
        if (tour != null && tour.getId() > 0 && user != null && user.getId() > 0) {
            return text == null || text.length() < 200;
        }
        return false;
    }

    /**
     * This method validates parameters needed for updating tour in the storage.
     *
     * @param id          unique tour identifier
     * @param country     country associated with this tour
     * @param hotel       hotel associated with this tour
     * @param type        type of the tour
     * @param name        name of the tour
     * @param description tour description
     * @param dateTime    timestamp when the tour begins
     * @param duration    duration of the tour in days
     * @param cost        cost of the tour
     * @return true if validation was successful, false otherwise
     */
    public static boolean validateTour(long id, Country country, Hotel hotel, TourType type, String name, String description,
                                       LocalDate dateTime, Period duration, BigDecimal cost) {
        return id > 0 && country != null && country.getId() > 0
                && hotel != null && hotel.getId() > 0
                && type != null
                && name != null
                && description != null
                && dateTime != null
                && duration != null && duration.getDays() > 0
                && cost != null;
    }

    /**
     * This method validates parameters needed for updating user in the storage.
     *
     * @param id        unique user identifier
     * @param login     login of the user
     * @param password  hash of the user password
     * @param email     email of the user
     * @param firstName first name of the user
     * @param lastName  last name of the user
     * @return true if validation was successful, false otherwise
     */
    public static boolean validateUser(long id, String login, String password, String email, String firstName, String lastName) {
        return id > 0 && login != null && password != null && email != null && firstName != null && lastName != null;
    }

    /**
     * This method validates unique identifier of an entity before deleting.
     *
     * @param id unique identifier
     * @return true if validation was successful, false otherwise
     */
    public static boolean validateId(long id) {
        return id > 0;
    }
}
