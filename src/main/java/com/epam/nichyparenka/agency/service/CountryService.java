package com.epam.nichyparenka.agency.service;

import com.epam.nichyparenka.agency.entity.Country;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The class provides logic for data manipulation with countries.
 * All logical methods associated with countries must be placed
 * in this class.
 *
 * @author Dzmitry Nichyparenka
 */
@Service
public class CountryService {
    private BasicRepository<Country> countryRepository;

    /**
     * This constructor creates the object of {@link CountryService}
     * with country repository as parameter.
     *
     * @param countryRepository repository for countries
     */
    public CountryService(BasicRepository<Country> countryRepository) {
        this.countryRepository = countryRepository;
    }

    /**
     * This method creates the country in the data storage.
     *
     * @param country country entity to create
     * @return true if operation was successful, false
     * otherwise
     */
    @Transactional
    public boolean createCountry(Country country) {
        return countryRepository.create(country);
    }

    /**
     * This method reads the country data from the data
     * storage.
     *
     * @param id unique identifier of the country
     * @return optional with country entity if it found
     * in the data storage, empty optional otherwise
     */
    public Optional<Country> readCountry(long id) {
        return countryRepository.read(id);
    }

    /**
     * This method updates country data in the data
     * storage.
     *
     * @param country country entity to update
     * @return true if operation was successful,
     * false otherwise
     */
    @Transactional
    public boolean updateCountry(Country country) {
        return countryRepository.update(country);
    }

    /**
     * This method deletes the country data if it
     * exists in the data storage.
     *
     * @param country country entity to delete
     */
    @Transactional
    public boolean deleteCountry(Country country) {
        return countryRepository.delete(country);
    }

    /**
     * This method deletes data of some countries
     * if these countries exist in the data storage.
     *
     * @param countries countries to delete
     */
    @Transactional(isolation = Isolation.REPEATABLE_READ, rollbackFor = BadQueryExecutionException.class)
    public void deleteCountries(Country... countries) throws BadQueryExecutionException {
        for (Country country : countries) {
            if (!countryRepository.delete(country)) {
                throw new BadQueryExecutionException("Transaction with deleting of countries has been canceled.");
            }
        }
    }

    /**
     * This method executes query on countries in data
     * storage which performs specific action.
     *
     * @param specificAction specific action on countries
     * @return list of entities, depends on specification
     * parameter
     */
    public List<Country> query(Specification specificAction) {
        return countryRepository.query(specificAction);
    }
}
