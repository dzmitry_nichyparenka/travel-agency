package com.epam.nichyparenka.agency.service;

import com.epam.nichyparenka.agency.entity.Review;
import com.epam.nichyparenka.agency.entity.Tour;
import com.epam.nichyparenka.agency.entity.User;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The class provides logic for data manipulation with tours.
 * All logical methods associated with tours must be placed
 * in this class.
 *
 * @author Dzmitry Nichyparenka
 */
@Service
public class TourService {
    private BasicRepository<Tour> tourRepository;
    private BasicRepository<Review> reviewRepository;
    private BasicRepository<User> userRepository;

    /**
     * This constructor creates the object of {@link TourService}
     * with tour repository as parameter.
     *
     * @param tourRepository   repository for tours
     * @param reviewRepository repository for reviews
     * @param userRepository   repository for users
     */
    public TourService(BasicRepository<Tour> tourRepository, BasicRepository<Review> reviewRepository, BasicRepository<User> userRepository) {
        this.tourRepository = tourRepository;
        this.reviewRepository = reviewRepository;
        this.userRepository = userRepository;
    }

    /**
     * This method creates the tour in the data storage.
     *
     * @param tour tour entity to create
     * @param user user, associated with this tour
     * @return true if operation was successful, false
     * otherwise
     */
    @Transactional(isolation = Isolation.REPEATABLE_READ, rollbackFor = BadQueryExecutionException.class)
    public boolean createTour(Tour tour, User user) throws BadQueryExecutionException {
        if (tourRepository.create(tour)) {
            Optional<User> userOptional = userRepository.read(user.getId());
            if (userOptional.isPresent()) {
                Review review = new Review();
                review.setTour(tour);
                review.setUser(userOptional.get());
                return reviewRepository.create(review);
            } else {
                if (userRepository.create(user)) {
                    Review review = new Review();
                    review.setTour(tour);
                    review.setUser(user);
                    return reviewRepository.create(review);
                }
            }
        }
        throw new BadQueryExecutionException("Transaction with creating new tour has been canceled.");
    }

    /**
     * This method reads the tour data from the data
     * storage.
     *
     * @param id unique identifier of the tour
     * @return optional with tour entity if it found
     * in the data storage, empty optional otherwise
     */
    public Optional<Tour> readTour(long id) {
        return tourRepository.read(id);
    }

    /**
     * This method updates tour data in the data
     * storage.
     *
     * @param tour tour entity to update
     * @return true if operation was successful,
     * false otherwise
     */
    @Transactional
    public boolean updateTour(Tour tour) {
        return tourRepository.update(tour);
    }

    /**
     * This method deletes the tour data if it
     * exists in the collection.
     *
     * @param tour tour entity to delete
     * @return true if operation was successful,
     * false otherwise
     */
    @Transactional
    public boolean deleteTour(Tour tour) {
        return tourRepository.delete(tour);
    }

    /**
     * This method deletes data of some tours
     * if these tours exist in the data storage.
     *
     * @param tours tours to delete
     */
    @Transactional(isolation = Isolation.REPEATABLE_READ, rollbackFor = BadQueryExecutionException.class)
    public void deleteTours(Tour... tours) throws BadQueryExecutionException {
        for (Tour tour : tours) {
            if (!tourRepository.delete(tour)) {
                throw new BadQueryExecutionException("Transaction with deleting of tours has been canceled.");
            }
        }
    }

    /**
     * This method executes query on tours in data
     * storage which performs specific action.
     *
     * @param specificAction specific action on tours
     * @return list of entities, depends on specification
     * parameter
     */
    public List<Tour> query(Specification specificAction) {
        return tourRepository.query(specificAction);
    }
}
