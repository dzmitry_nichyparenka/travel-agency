package com.epam.nichyparenka.agency.service;

import com.epam.nichyparenka.agency.entity.Review;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The class provides logic for data manipulation with reviews.
 * All logical methods associated with reviews must be placed
 * in this class.
 *
 * @author Dzmitry Nichyparenka
 */
@Service
public class ReviewService {
    private BasicRepository<Review> reviewRepository;

    /**
     * This constructor creates the object of {@link ReviewService}
     * with review repository as parameter.
     *
     * @param reviewRepository repository for reviews
     */
    public ReviewService(BasicRepository<Review> reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    /**
     * This method creates the review in the data storage.
     *
     * @param review review entity to create
     * @return true if operation was successful, false
     * otherwise
     */
    @Transactional
    public boolean createReview(Review review) {
        return reviewRepository.create(review);
    }

    /**
     * This method reads the review data from the data
     * storage.
     *
     * @param id unique identifier of the review
     * @return optional with review entity if it found
     * in the data storage, empty optional otherwise
     */
    public Optional<Review> readReview(long id) {
        return reviewRepository.read(id);
    }

    /**
     * This method updates review data in the data
     * storage.
     *
     * @param review review entity to update
     * @return true if operation was successful,
     * false otherwise
     */
    @Transactional
    public boolean updateReview(Review review) {
        return reviewRepository.update(review);
    }

    /**
     * This method deletes the review data if it
     * exists in the collection.
     *
     * @param review review entity to delete
     * @return true if operation was successful,
     * false otherwise
     */
    @Transactional
    public boolean deleteReview(Review review) {
        return reviewRepository.delete(review);
    }

    /**
     * This method deletes data of some reviews
     * if these reviews exist in the data storage.
     *
     * @param reviews reviews to delete
     */
    @Transactional(isolation = Isolation.REPEATABLE_READ, rollbackFor = BadQueryExecutionException.class)
    public void deleteReviews(Review... reviews) throws BadQueryExecutionException {
        for (Review review : reviews) {
            if (!reviewRepository.delete(review)) {
                throw new BadQueryExecutionException("Transaction with deleting of reviews has been canceled.");
            }
        }
    }

    /**
     * This method executes query on reviews in data
     * storage which performs specific action.
     *
     * @param specificAction specific action on reviews
     * @return list of entities, depends on specification
     * parameter
     */
    public List<Review> query(Specification specificAction) {
        return reviewRepository.query(specificAction);
    }
}
