package com.epam.nichyparenka.agency.service;

import com.epam.nichyparenka.agency.entity.User;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The class provides logic for data manipulation with users.
 * All logical methods associated with users must be placed
 * in this class.
 *
 * @author Dzmitry Nichyparenka
 */
@Service
@Transactional
public class UserService {
    private BasicRepository<User> userRepository;

    /**
     * This constructor creates the object of {@link UserService}
     * with user repository as parameter.
     *
     * @param userRepository repository for users
     */
    public UserService(BasicRepository<User> userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * This method creates the user in the data storage.
     *
     * @param user user entity to create
     * @return true if operation was successful, false
     * otherwise
     */
    @Transactional
    public boolean createUser(User user) {
        return userRepository.create(user);
    }

    /**
     * This method reads the user data from the data
     * storage.
     *
     * @param id unique identifier of the user
     * @return optional with user entity if it found
     * in the data storage, empty optional otherwise
     */
    public Optional<User> readUser(long id) {
        return userRepository.read(id);
    }

    /**
     * This method updates user data in the data
     * storage.
     *
     * @param user user entity to update
     * @return true if operation was successful,
     * false otherwise
     */
    @Transactional
    public boolean updateUser(User user) {
        return userRepository.update(user);
    }

    /**
     * This method deletes the user data if it
     * exists in the data storage.
     *
     * @param user user entity to delete
     * @return true if operation was successful,
     * false otherwise
     */
    @Transactional
    public boolean deleteUser(User user) {
        return userRepository.delete(user);
    }

    /**
     * This method deletes data of some users
     * if these users exist in the data storage.
     *
     * @param users users to delete
     */
    @Transactional(isolation = Isolation.REPEATABLE_READ, rollbackFor = BadQueryExecutionException.class)
    public void deleteUsers(User... users) throws BadQueryExecutionException {
        for (User user : users) {
            if (!userRepository.delete(user)) {
                throw new BadQueryExecutionException("Transaction with deleting of users has been canceled.");
            }
        }
    }

    /**
     * This method executes query on users in the data
     * storage which performs specific action.
     *
     * @param specificAction specific action on users
     * @return list of entities, depends on specification
     * parameter
     */
    public List<User> query(Specification specificAction) {
        return userRepository.query(specificAction);
    }
}
