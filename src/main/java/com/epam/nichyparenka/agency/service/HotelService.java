package com.epam.nichyparenka.agency.service;

import com.epam.nichyparenka.agency.entity.Hotel;
import com.epam.nichyparenka.agency.exception.BadQueryExecutionException;
import com.epam.nichyparenka.agency.repository.BasicRepository;
import com.epam.nichyparenka.agency.repository.specification.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The class provides logic for data manipulation with hotels.
 * All logical methods associated with hotels must be placed
 * in this class.
 *
 * @author Dzmitry Nichyparenka
 */
@Service
public class HotelService {
    private BasicRepository<Hotel> hotelRepository;

    /**
     * This constructor creates the object of {@link HotelService}
     * with hotel repository as parameter.
     *
     * @param hotelRepository repository for hotels
     */
    public HotelService(BasicRepository<Hotel> hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    /**
     * This method creates the hotel in the data storage.
     *
     * @param hotel hotel entity to create
     * @return true if operation was successful, false
     * otherwise
     */
    @Transactional
    public boolean createHotel(Hotel hotel) {
        return hotelRepository.create(hotel);
    }

    /**
     * This method reads the hotel data from the data
     * storage.
     *
     * @param id unique identifier of the hotel
     * @return optional with hotel entity if it found
     * in the data storage, empty optional otherwise
     */
    public Optional<Hotel> readHotel(long id) {
        return hotelRepository.read(id);
    }

    /**
     * This method updates hotel data in the data
     * storage.
     *
     * @param hotel hotel entity to update
     * @return true if operation was successful,
     * false otherwise
     */
    @Transactional
    public boolean updateHotel(Hotel hotel) {
        return hotelRepository.update(hotel);
    }

    /**
     * This method deletes the hotel data if it
     * exists in the collection.
     *
     * @param hotel hotel entity to delete
     * @return true if operation was successful,
     * false otherwise
     */
    @Transactional
    public boolean deleteHotel(Hotel hotel) {
        return hotelRepository.delete(hotel);
    }

    /**
     * This method deletes data of some hotels
     * if these hotels exist in the data storage.
     *
     * @param hotels hotels to delete
     */
    @Transactional(isolation = Isolation.REPEATABLE_READ, rollbackFor = BadQueryExecutionException.class)
    public void deleteHotels(Hotel... hotels) throws BadQueryExecutionException {
        for (Hotel hotel : hotels) {
            if (!hotelRepository.delete(hotel)) {
                throw new BadQueryExecutionException("Transaction with deleting of hotels has been canceled.");
            }
        }
    }

    /**
     * This method executes query on hotels in data
     * storage which performs specific action.
     *
     * @param specificAction specific action on hotels
     * @return list of entities, depends on specification
     * parameter
     */
    public List<Hotel> query(Specification specificAction) {
        return hotelRepository.query(specificAction);
    }
}
